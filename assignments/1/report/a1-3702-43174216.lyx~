#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{multicol}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 2cm
\rightmargin 1cm
\bottommargin 2cm
\headheight 1cm
\headsep 1cm
\footskip 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
COMP3702 Assignment 1
\end_layout

\begin_layout Author
Thomas Hines
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

43174216
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{multicols}{2}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $V$
\end_inset

 is the set of all vertices.
\end_layout

\begin_layout Standard
\begin_inset Formula $V_{V}$
\end_inset

 is the set of visited vertices.
\end_layout

\begin_layout Standard
\begin_inset Formula $V_{U}$
\end_inset

 is the set of unvisited vertices.
\end_layout

\begin_layout Standard
\begin_inset Formula $V_{F}$
\end_inset

 is the set of fringe vertices.
\end_layout

\begin_layout Standard
\begin_inset Formula $V_{a}$
\end_inset

 is the set of vertices connected from 
\begin_inset Formula $a$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $V_{U,a}$
\end_inset

 is the set of unvisited vertices connected from 
\begin_inset Formula $a$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $E$
\end_inset

 is the set of edges
\end_layout

\begin_layout Standard
\begin_inset Formula $s$
\end_inset

 is the initial vertex.
\end_layout

\begin_layout Standard
\begin_inset Formula $d$
\end_inset

 is the goal vertex.
\end_layout

\begin_layout Standard
\begin_inset Formula $\overline{ab'}$
\end_inset

 is the edge from 
\begin_inset Formula $a$
\end_inset

 to 
\begin_inset Formula $b$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $\left|\overline{ab'}\right|$
\end_inset

 is the cost of the edge from 
\begin_inset Formula $a$
\end_inset

 to 
\begin_inset Formula $b$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $g(a)$
\end_inset

 is the lowest calculated cost from 
\begin_inset Formula $s$
\end_inset

 to 
\begin_inset Formula $a$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Formula $g_{ab}$
\end_inset

 is the actual cost from 
\begin_inset Formula $a$
\end_inset

 to 
\begin_inset Formula $b$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{multicols}
\end_layout

\end_inset


\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:agent-def"

\end_inset

What is the formal definition of the navigation agent in this assignment?
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="3">
<features tabularvalignment="middle">
<column alignment="right" valignment="middle" width="4cm">
<column alignment="left" valignment="middle" width="6cm">
<column alignment="left" valignment="middle" width="8cm">
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Description
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Definition
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
State space
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The state includes the structure of the graph: the set of vertices and the
 edges between them.
\end_layout

\begin_layout Plain Layout
Actions change the contents of the visited, unvisited and fringe sets as
 well as the lowest calculated costs.
\end_layout

\begin_layout Plain Layout
Initially, all the lowest calculated costs are infinite, except for 
\series bold

\begin_inset Formula $s$
\end_inset


\series default
, which starts at 0.
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
State space, 
\begin_inset Formula $S$
\end_inset

, contains:
\end_layout

\begin_layout Plain Layout
The set of vertices: 
\begin_inset Formula $V$
\end_inset

.
\end_layout

\begin_layout Plain Layout
The edges and their costs: 
\begin_inset Formula $E$
\end_inset

.
\end_layout

\begin_layout Plain Layout
The visited, unvisited and fringe sets: 
\begin_inset Formula $V_{V}$
\end_inset

, 
\begin_inset Formula $V_{U}$
\end_inset

, 
\begin_inset Formula $V_{F}$
\end_inset

.
\end_layout

\begin_layout Plain Layout
The lowest calculated vertex costs: 
\begin_inset Formula $g(a),a\in V$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Percept function
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The percept function returns the complete state.
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Percept function, 
\begin_inset Formula $P=Z(S)$
\end_inset

, is:
\end_layout

\begin_layout Plain Layout
\begin_inset Formula $P=Z(S)=S$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Percept space
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The agent can see the complete state.
 Including the graph structure, set contents, and lowest calculated costs.
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Percept space, 
\begin_inset Formula $P$
\end_inset

, contains:
\end_layout

\begin_layout Plain Layout
The complete state: 
\begin_inset Formula $S$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Action space
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The agent can choose to visit any fringe vertex.
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Action space, 
\begin_inset Formula $A$
\end_inset

, contains:
\end_layout

\begin_layout Plain Layout
Visit any 
\begin_inset Formula $a,a\in V_{F}$
\end_inset

.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
World dynamics
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
When a fringe vertex is visited, it is removed from the fringe and unvisited
 vertex sets and added to the visited vertex set.
\end_layout

\begin_layout Plain Layout
Then, the cost to each of its unvisited connected vertices is calculated
 and stored if the new cost is less than the old cost.
\end_layout

\begin_layout Plain Layout
Finally, the unvisited connected vertices are added to the fringe vertex
 set.
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Visit vertex 
\begin_inset Formula $a$
\end_inset

:
\end_layout

\begin_layout Plain Layout
\begin_inset Formula $V_{U}=V_{U}\backslash\{a\}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Formula $V_{V}=V_{V}\cup\{a\}$
\end_inset


\end_layout

\begin_layout Plain Layout
Update the vertex costs in 
\begin_inset Formula $V_{U,a}$
\end_inset

:
\end_layout

\begin_layout Plain Layout
\begin_inset Formula $g(b)=min\{g(b),g(a)+\left|\overline{ab'}\right|\},b\in V_{U,a}$
\end_inset


\end_layout

\begin_layout Plain Layout
Remove 
\begin_inset Formula $a$
\end_inset

 from the fringe set:
\end_layout

\begin_layout Plain Layout
\begin_inset Formula $V_{F}=V_{F}\setminus\{a\}$
\end_inset


\end_layout

\begin_layout Plain Layout
Add 
\begin_inset Formula $V_{U,a}$
\end_inset

 to the fringe set:
\end_layout

\begin_layout Plain Layout
\begin_inset Formula $V_{F}=V_{F}\cup V_{U,a}$
\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Section
What type of agent is the navigation agent as described in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "sec:agent-def"

\end_inset

 (i.e., discrete/continuous, fully/partially observable, deterministic/non-determi
nistic, static/dynamic)? Please explain your selection.
\end_layout

\begin_layout Standard
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="2">
<features tabularvalignment="middle">
<column alignment="right" valignment="middle" width="4cm">
<column alignment="left" valignment="middle" width="14cm">
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Reason
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Discrete
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
There are a finite number of states.
 The agent performs individual actions.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Fully observable
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The percept function returns the entire state space.
 No part of the environment is unobservable.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Deterministic
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
No part of the environment and its behaviour is unknown.
 The agent's actions are the only inputs to the system.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="right" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Static
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="middle" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
The agent's actions are the only inputs to the system so it will not change
 when the agent is thinking.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Section
What is the heuristic you used for A* search? Please explain why do you
 think it is a good heuristic?
\end_layout

\begin_layout Standard
The heuristic, 
\begin_inset Formula $h(a)$
\end_inset

 is defined for vertex 
\begin_inset Formula $a$
\end_inset

 as:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)=\begin{cases}
min\{\left|\overline{ab'}\right|,b\in V_{a}\} & a\neq d\\
0 & a=d
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
An A* heuristic must be admissible to ensure optimality.
 It should also be consistent to make revisiting vertices unnecessary.
\end_layout

\begin_layout Subsection*
Admissibility
\end_layout

\begin_layout Standard
The heuristic must never overestimate the cost to get from a vertex to the
 destination.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)\leq g_{ad}
\]

\end_inset


\end_layout

\begin_layout Standard
If the vertex, 
\begin_inset Formula $a$
\end_inset

, is the destination, 
\begin_inset Formula $d$
\end_inset

, then the cost, 
\begin_inset Formula $g_{ad}$
\end_inset

 is:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
g_{ad}=0
\]

\end_inset


\end_layout

\begin_layout Standard
The heuristic function returns:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)=0
\]

\end_inset


\end_layout

\begin_layout Standard
Since 
\begin_inset Formula $0\leq0$
\end_inset

, the heuristic is admissible in this case.
\end_layout

\begin_layout Standard
If the vertex is not the destination, then the destination must be reached
 by leaving this vertex, therefore the cost 
\begin_inset Formula $g_{ad}$
\end_inset

 must be at least the cost of the cheapest edge leaving 
\begin_inset Formula $a$
\end_inset

:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
g_{ad}\geq min\{\left|\overline{ab'}\right|,b\in V_{a}\}
\]

\end_inset


\end_layout

\begin_layout Standard
The right side of the above equation is the definition of the heuristic
 function in this case, so:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
g_{ad}\geq h(a)
\]

\end_inset


\end_layout

\begin_layout Standard
Therefore, the heuristic is admissible.
\end_layout

\begin_layout Subsection*
Consistency
\end_layout

\begin_layout Standard
The heuristic value of a parent vertex must be less than the heuristic of
 each of its child vertices plus the cost to get to that vertex.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)\leq\left|\overline{ab'}\right|+h(b),b\in V_{a}
\]

\end_inset


\end_layout

\begin_layout Standard
From the problem definition we know that all edges must be greater than
 0.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|\overline{ab'}\right|\geq0
\]

\end_inset


\end_layout

\begin_layout Standard
Since 
\begin_inset Formula $h(a)$
\end_inset

 is defined as either 0 or the minimum edge departing 
\begin_inset Formula $a$
\end_inset

 and 
\begin_inset Formula $c_{ab}$
\end_inset

 is an edge departing 
\begin_inset Formula $a$
\end_inset

, we know that:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)\leq\left|\overline{ab'}\right|
\]

\end_inset


\end_layout

\begin_layout Standard
Therefore:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)\geq0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(b)\geq0
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
h(a)\leq\left|\overline{ab'}\right|+h(b)
\]

\end_inset


\end_layout

\begin_layout Standard
So the heuristic is consistent.
\end_layout

\begin_layout Section
Please compare the performance (in terms of time and space) of Uniform Cost
 and A* search as the number of vertices in the graph increases.
 Please explain your findings.
 This explanation should include comparisons with the theoretical results.
\end_layout

\begin_layout Subsection*
Theoretical performance
\end_layout

\begin_layout Subsubsection*
Uniform Cost
\end_layout

\begin_layout Standard
The performance of the Uniform Cost Search (UCS) is dependent on the graph
 and its connectivity.
 Let 
\begin_inset Formula $b$
\end_inset

 be the branching factor: the average number of edges departing from each
 vector.
 On the first iteration, UCS expands the first b vectors.
 Then, assuming a roughly average spread of costs, it will expand b vectors
 from each of those.
 The total number of expanded vectors up to iteration 
\begin_inset Formula $d$
\end_inset

 is the sum of a series:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
v=b+b^{2}+b^{3}+...+b^{d}=\frac{b^{d+1}-1}{b-1}
\]

\end_inset


\end_layout

\begin_layout Standard
If the time complexity is relative to the number of expanded vectors, then
 UCS is 
\begin_inset Formula $O(b^{d})$
\end_inset

.
\end_layout

\begin_layout Standard
This can be expanded on by investigating the parameters that determine the
 number of levels UCS must go down to reach its target.
\end_layout

\begin_layout Subsubsection*
A*
\end_layout

\begin_layout Standard
If A* runs on a perfect heuristic, it will expand the optimal route only.
 In this case, the number of operations is relative to the product of the
 branching factor and the length of the optimal route.
 Therefore, A* is best case 
\begin_inset Formula $O(bd)$
\end_inset

.
 With no heuristic, it is the same as a uniform cost search, running at
 
\begin_inset Formula $O(b^{d})$
\end_inset

.
 The actual performance, once again, depends on the characteristics of the
 graph.
\end_layout

\begin_layout Standard
Since these complexities were modelled using the number of expanded nodes,
 the two should have memory usage similar to these limits.
\end_layout

\begin_layout Subsection*
Implementation performance
\end_layout

\begin_layout Standard
The running time of the 2 algorithms are modelled on a graph with 
\begin_inset Formula $\left|V\right|$
\end_inset

 vertices and 
\begin_inset Formula $\left|E\right|$
\end_inset

 edges.
 Both algorithms increase in run time with the number of vertices at a similar
 rate as can be seen in 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Run-time-vs"

\end_inset

.
 However, Uniform Cost search suffered much more from increasing the number
 of edges in the graph as can be seen in 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Run-time-vs-1"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space \hfill{}
\end_inset


\begin_inset Graphics
	filename ../data/data.vert.png
	lyxscale 50
	width 100text%

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Run-time-vs"

\end_inset

Run time vs edge count.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space \hfill{}
\end_inset


\begin_inset Graphics
	filename ../data/data.edge.png
	lyxscale 50
	width 100text%

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Run-time-vs-1"

\end_inset

Run time vs edge count.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
This suggests that the A* algorithm is effective at dealing with increasing
 branching factor and has a smaller complexity dependent on it.
\end_layout

\begin_layout Subsection*
Comparison
\end_layout

\begin_layout Standard
The asymptotic complexity models were both dependent on the branching factor,
 which should be relative to the number of edges.
\end_layout

\begin_layout Section
Suppose you are given 2 environment maps, say A and B.
 True or False that if the number of vertices in A is larger than in B,
 then given the same implementation of A* search, finding an optimal path
 in A will always take longer time than in B? Please explain your answer.
\end_layout

\begin_layout Standard
Given:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\left|V\right|_{A}>\left|V\right|_{B}
\]

\end_inset


\end_layout

\begin_layout Standard
Prove or disprove:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
runtime_{A}>runtime_{B}
\]

\end_inset


\end_layout

\begin_layout Standard
This can easily be proved false using logic.
 If:
\end_layout

\begin_layout Itemize
A and B are the same maps but with one extra vertex, 
\begin_inset Formula $c$
\end_inset

 and edges 
\begin_inset Formula $\overline{ac'}$
\end_inset

 and 
\begin_inset Formula $\overline{cb'}$
\end_inset

;
\end_layout

\begin_layout Itemize
and the cost 
\begin_inset Formula $\left|\overline{ac'}\right|+\left|\overline{cb'}\right|$
\end_inset

 is less than the route that would have been required in map A.
\end_layout

\begin_layout Standard
Then, when the algorithm is given the task of finding a route from 
\begin_inset Formula $a$
\end_inset

 to 
\begin_inset Formula $b$
\end_inset

, the new vertex could provide a shorter path than was otherwise available.
 Therefore the answer is false.
\end_layout

\end_body
\end_document
