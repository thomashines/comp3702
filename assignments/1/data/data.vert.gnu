#!/usr/bin/env gnuplot

# Output
set output "data.vert.png"

# Key
set bmargin at screen 0.2
set key bmargin left
set key box black
set key horizontal

# Labels
set title "Runtime vs vertices"
set xlabel "Vertices"
set ylabel "Runtime (ms)"

# Axes
set tics out

# Plot
set multiplot
plot "data.vert.uniform.csv" using 2:4 title "Uniform", \
       "data.vert.astar.csv" using 2:4 title "A*"
