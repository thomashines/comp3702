#!/usr/bin/env python

import subprocess

vrange = [100, 1000, 50]
vconst = vrange[1]
erange = [100, 5000, 100]
econst = erange[0]
envs = 1
queries = 8

# X=Vertices, Y=Duration
edges = int((vrange[0] ** 2) / 2)
for vertices in range(*vrange):
    subprocess.call(["./generate.py"] + [str(arg) for arg in [vertices, edges, envs, queries]])
    ext = "V%d-E%d" % (vertices, edges)
    subprocess.call(["./a1-3702-43174216", "env/env-%s.0.txt" % ext, "query/query-%s.txt" % ext, "out/out.txt"])

# X=Edges, Y=Duration
# vertices = 2 * int(erange[1] ** 0.5)
# for edges in range(*erange):
#     subprocess.call(["./generate.py"] + [str(arg) for arg in [vertices, edges, envs, queries]])
#     ext = "V%d-E%d" % (vertices, edges)
#     subprocess.call(["./a1-3702-43174216", "env/env-%s.0.txt" % ext, "query/query-%s.txt" % ext, "out/out.txt"])
