#!/usr/bin/env gnuplot

# Output
set output "data.edge.png"

# Key
set bmargin at screen 0.2
set key bmargin left
set key box black
set key horizontal

# Labels
set title "Runtime vs edges"
set xlabel "Edges"
set ylabel "Runtime (ms)"

# Axes
set tics out

# Plot
set multiplot
plot "data.edge.uniform.csv" using 3:4 title "Uniform", \
       "data.edge.astar.csv" using 3:4 title "A*"
