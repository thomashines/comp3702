#!/usr/bin/env python

import random
import os
import sys

# Get arguments
if len(sys.argv) != 5:
    print("./generate.py <vertices> <edges> <envs> <queries>")
    exit()
vertices = int(sys.argv[1])
edges = int(sys.argv[2])
envs = int(sys.argv[3])
queries = int(sys.argv[4])
# print("generating", (vertices, edges, envs, queries))

if edges > vertices**2:
    edges = vertices**2

# Get paths
env_path = os.path.join(os.getcwd(), "env")
# print("envs in", env_path)
query_path = os.path.join(os.getcwd(), "query")
# print("queries in", query_path)

# Generate environments
for env in range(envs):
    graph = []
    connections = []
    while len(graph) < edges:
        edge = random.sample(range(vertices), 2)
        if edge not in connections:
            connections.append(edge)
            graph.append((edge, random.uniform(0.5, 1.0)))
    connections = [[0 for t in range(vertices)] for f in range(vertices)]
    for edge in graph:
        connections[edge[0][0]][edge[0][1]] = edge[1]
    with open(os.path.join(env_path, "env-V%d-E%d.%d.txt" % (vertices, edges, env)), "w") as env_file:
        env_file.write("%d\n" % vertices)
        env_file.write("\n".join([" ".join(["%.2f" % t for t in f]) for f in connections]))
        env_file.write("\n")

# Generate queries
connections = []
while len(connections) < queries:
    connection = random.sample(range(vertices), 2)
    if connection not in connections:
        connections.append(connection)
with open(os.path.join(query_path, "query-V%d-E%d.txt" % (vertices, edges)), "w") as query_file:
    query_file.write("%d\n" % (2 * queries))
    for c in connections:
        query_file.write("Uniform %d %d\n" % (c[0], c[1]))
        # query_file.write("A* %d %d\n" % (c[0], c[1]))
