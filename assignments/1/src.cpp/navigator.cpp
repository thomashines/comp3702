#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <streambuf>
#include <string>
#include <vector>
#include <queue>

using namespace std;

// Infinity
float float_inf = numeric_limits<float>::max();

// Vertex
class Vertex {
    public:
        // Graph structure
        int index;
        int edge_count;
        vector<Vertex *> edges;
        vector<float> edge_costs;

        // Search details
        bool visited;
        float cost_f; // sort metric, f = g + h
        float cost_g; // actual cost from initial
        float cost_h; // heuristic cost to goal

        // Route details
        Vertex *route_from;
        float route_from_cost;

        Vertex(void);
        void set_index(int index);
        void connect_to(Vertex *vertex, float cost);
        string route_string(int *depth);
        ~Vertex(void);
};

Vertex::Vertex(void) {
    edge_count = 0;
    cost_h = float_inf;
}

void Vertex::set_index(int index) {
    this->index = index;
}

void Vertex::connect_to(Vertex *vertex, float cost) {
    edge_count++;
    edges.push_back(vertex);
    edge_costs.push_back(cost);
    if (cost < cost_h) {
        cost_h = cost;
    }
}

string Vertex::route_string(int *depth) {
    if (route_from) {
        (*depth)++;
        return route_from->route_string(depth) + "-" + to_string(index);
        // return route_from->route_string(depth) + "-(" + to_string(route_from_cost) + ")->" + to_string(index);
    }
    return to_string(index);
}

ostream& operator<<(ostream &strm, const Vertex &vertex) {
  return strm << "<" << vertex.index << " (" << vertex.cost_f << ")" << ">";
}

Vertex::~Vertex(void) {
}

struct VertexComparator {
    public:
        bool operator()(const Vertex *lhs, const Vertex *rhs) {
            return lhs->cost_f >= rhs->cost_f;
        }
};

// Navigation
int navigate(vector<Vertex> *vertices_ptr, int vertex_count, int from, int to,
        float (*heuristic)(Vertex *vertex, int to));
float heuristic_uniform(Vertex *vertex, int to);
float heuristic_astar(Vertex *vertex, int to);

// Main
int main(int argc, char **argv) {
    // Check arguments
    if (argc != 4) {
        cout << "./a1-3702-43174216 <env file> <query file> <output file>";
        cout << endl;
    }

    // Set up clock
    clock_t before, after;
    float duration;
    before = clock();

    // Load environment file into string
    ifstream env_stream(argv[1]);
    env_stream.seekg(0, ios::end);
    size_t env_size = env_stream.tellg();
    string env_string(env_size, ' ');
    env_stream.seekg(0);
    env_stream.read(&env_string[0], env_size);
    // cout << env_string;

    // Get vertex count
    stringstream env_converter(env_string);
    int vertex_count;
    env_converter >> vertex_count;
    // cout << vertex_count << " vertices" << endl;

    // Make vertex list
    vector<Vertex> vertices(vertex_count, Vertex());
    float cost;
    int edge_count = 0;
    for (int from = 0; from < vertex_count; from++) {
        // Set index
        vertices[from].set_index(from + 1);

        // Read edges
        for (int to = 0; to < vertex_count; to++) {
            env_converter >> cost;
            if (cost > 0) {
                // cout << "connecting " << from + 1 << " to " << to + 1;
                // cout << " for " << cost << endl;
                vertices[from].connect_to(&vertices[to], cost);
                edge_count++;
            }
        }
    }

    // Load query file into string
    ifstream query_stream(argv[2]);
    query_stream.seekg(0, ios::end);
    size_t query_size = query_stream.tellg();
    string query_string(query_size, ' ');
    query_stream.seekg(0);
    query_stream.read(&query_string[0], query_size);
    // cout << query_string;

    // Get query count
    stringstream query_converter(query_string);
    int query_count;
    query_converter >> query_count;
    // cout << query_count << " queries" << endl;

    // Open output file
    ofstream out_stream;
    out_stream.open(argv[3]);
    out_stream.setf(ios::fixed, ios::floatfield);
    out_stream.precision(2);

    // Time startup
    after = clock();
    duration = 1000 * (after - before) / (double) CLOCKS_PER_SEC;
    cout << "startup " << duration << "ms" << endl;

    // Do queries
    string method;
    int from;
    int to;
    int expanded;
    int depth;
    string route;
    for (int query = 0; query < query_count; query++) {
        // Parse query
        query_converter >> method;
        query_converter >> from;
        query_converter >> to;

        // Start timing
        before = clock();

        // Do navigation
        if (method == "Uniform") {
            expanded = navigate(&vertices, vertex_count, from, to,
                heuristic_uniform);
        } else if (method == "A*") {
            expanded = navigate(&vertices, vertex_count, from, to,
                heuristic_astar);
        }

        // Stop timing
        after = clock();
        duration = 1000 * (after - before) / (double) CLOCKS_PER_SEC;

        // Get route
        depth = 0;
        route = vertices[to - 1].route_string(&depth);

        // Print result
        cout << method << " " << from << "->" << to << endl;
        // cout << "\troute " << route << endl;
        cout << "\tcost " << vertices[to - 1].cost_g << endl;
        cout << "\tdepth " << depth << endl;
        cout << "\texpanded " << expanded << endl;
        cout << "\tduration " << duration << "ms" << endl;

        // Print result CSV
        // if (vertices[to - 1].cost_g < float_inf) {
        //     cout << "\"" << method << "\", ";
        //     cout << vertex_count << ", ";
        //     cout << edge_count << ", ";
        //     cout << duration << ", ";
        //     cout << depth << ", ";
        //     cout << expanded << ", ";
        //     cout << vertices[to - 1].cost_g << ", ";
        //     cout << endl;
        // }

        // Write to output file
        out_stream << route;
        // out_stream << " cost " << vertices[to - 1].cost_g;
        // out_stream << " at depth " << depth;
        out_stream << endl;
    }

    return 0;
}

int navigate(vector<Vertex> *vertices_ptr, int vertex_count, int from, int to,
        float (*heuristic)(Vertex *vertex, int to)) {
    vector<Vertex> &vertices = *vertices_ptr;
    // Clear state
    for (int i = 0; i < vertex_count; i++) {
        vertices[i].visited = false;
        vertices[i].cost_f = float_inf;
        vertices[i].cost_g = float_inf;
        vertices[i].route_from = NULL;
        vertices[i].route_from_cost = float_inf;
    }

    // Set the initial vertex to 0 cost
    vertices[from - 1].cost_g = 0;
    vertices[from - 1].cost_f = heuristic(&vertices[from - 1], to);

    // Make fringe queue
    priority_queue<Vertex *, vector<Vertex *>, VertexComparator> fringe;
    fringe.push(&vertices[from - 1]);

    // For at most every node
    int expanded = 0;
    while (fringe.size() > 0) {
        // Print queue
        // cout << "fringe" << endl << "\t";
        // priority_queue<Vertex *, vector<Vertex *>, VertexComparator> tmp;
        // while (fringe.size() > 0) {
        //     Vertex *cheapest = fringe.top();
        //     cout << *cheapest << ", ";
        //     fringe.pop();
        //     tmp.push(cheapest);
        // }
        // cout << endl;
        // fringe.swap(tmp);

        // Get and remove the cheapest fringe node
        Vertex *cheapest = fringe.top();
        fringe.pop();

        // Check if it is the destination
        if (cheapest->index == to) {
            // cout << "found goal" << endl;
            break;
        }

        // Check if it hasn't been visited
        if (!cheapest->visited) {
            // Visit it
            // cout << "visiting " << *cheapest << endl;
            cheapest->visited = true;
            expanded++;

            // For each of its neighbours
            for (int n = 0; n < cheapest->edge_count; n++) {
                // Get the neighbour
                Vertex *neighbour = cheapest->edges[n];

                // Check if it hasn't been visited
                if (!neighbour->visited) {
                    // Calculate new cost
                    float new_cost_g = cheapest->cost_g +
                        cheapest->edge_costs[n];

                    // Compare to current distance
                    if (new_cost_g < neighbour->cost_g) {
                        // Copy across new route
                        neighbour->cost_g = new_cost_g;
                        neighbour->route_from = cheapest;
                        neighbour->route_from_cost = cheapest->edge_costs[n];

                        // Update sort cost with heuristic
                        neighbour->cost_f = heuristic(neighbour, to);

                        // Push it to fringe queue
                        fringe.push(neighbour);
                    }
                }
            }
        }
    }

    return expanded;
}

float heuristic_uniform(Vertex *vertex, int to) {
    return vertex->cost_g;
}

float heuristic_astar(Vertex *vertex, int to) {
    if (vertex->index == to) {
        return vertex->cost_g;
    }
    return vertex->cost_g + vertex->cost_h;
}
