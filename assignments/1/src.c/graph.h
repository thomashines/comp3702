#ifndef GRAPH_H
#define GRAPH_H

#include "node.h"

typedef struct {
    Node *nodes;
    int node_count;
} Graph;

Graph *graph_init(char *environment_file);
void graph_navigate_uniform(FILE *stream, Graph *graph, int from, int to);
void graph_navigate_astar(FILE *stream, Graph *graph, int from, int to);

#endif
