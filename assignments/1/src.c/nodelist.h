#ifndef NODELIST_H
#define NODELIST_H

#include "node.h"

typedef struct NodeList {
    Node *node;
    struct NodeList *nodelist;
} NodeList;

NodeList *nodelist_free(NodeList *nodelist);
NodeList *nodelist_push(NodeList *nodelist, Node *node);
NodeList *nodelist_push_sorted(NodeList *nodelist, Node *node);
NodeList *nodelist_remove(NodeList *nodelist, Node *node);
NodeList *nodelist_sort(NodeList *nodelist);
NodeList *nodelist_pop_first(NodeList *nodelist, Node **node);
Node *nodelist_get(NodeList *nodelist, int index);
Node *nodelist_get_highest(NodeList *nodelist);
Node *nodelist_get_lowest(NodeList *nodelist);
int nodelist_count(NodeList *nodelist);
int nodelist_contains(NodeList *nodelist, Node *node);
void nodelist_fprint(FILE *stream, NodeList *nodelist, int count);

#endif
