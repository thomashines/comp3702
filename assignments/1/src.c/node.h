#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    // Structure
    int index;
    int to_node_count;
    struct Node **to_nodes;
    float *to_node_costs;

    // Search
    char visited;
    struct Node *best_from_node;
    float best_from_node_cost;
    float distance_total; // f(n) = g(n) + h(n)
    float distance; // g(n)
    float heuristic; // h(n)

    // A*
    float to_node_costs_min;
} Node;

Node *node_init();
void node_free(Node *node);
void node_connect(Node *from_node, Node *to_node, float cost);
void node_fprint(FILE *stream, Node *node);
int node_route_fprint(FILE *stream, Node *node);
void node_route_costs_fprint(FILE *stream, Node *node);

#endif
