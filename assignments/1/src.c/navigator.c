#include <stdio.h>
#include <string.h>
#include <time.h>

#include "node.h"
#include "graph.h"

int main(int argc, char **argv) {
    // Set up timer
    struct timespec before;
    struct timespec after;

    // Time startup
    clock_gettime(CLOCK_MONOTONIC, &before);

    // TODO: check arguments

    // Make graph
    Graph *graph = graph_init(argv[1]);

    // Open output
    FILE *output = fopen(argv[3], "w");

    // Read queries
    FILE *queries = fopen(argv[2], "r");

    // Read node count
    int query_count;
    fscanf(queries, "%d\n", &query_count);
    printf("%d queries\n", query_count);

    // Time startup
    clock_gettime(CLOCK_MONOTONIC, &after);
    printf("startup %ldms\r\n",
        (after.tv_sec - before.tv_sec) * ((long int) 1E+3) +
        (after.tv_nsec - before.tv_nsec) / ((long int) 1E+6));

    // Read queries
    char method[10];
    int from, to;
    while (fscanf(queries, "%s %d %d\n", method, &from, &to) > 0) {
        clock_gettime(CLOCK_MONOTONIC, &before);
        if (strcmp(method, "Uniform") == 0) {
            graph_navigate_uniform(output, graph, from, to);
        } else if (strcmp(method, "A*") == 0) {
            graph_navigate_astar(output, graph, from, to);
        }
        clock_gettime(CLOCK_MONOTONIC, &after);
        printf("query %ldms\r\n",
            (after.tv_sec - before.tv_sec) * ((long int) 1E+3) +
            (after.tv_nsec - before.tv_nsec) / ((long int) 1E+6));
    }

    // Close
    fclose(queries);
    fclose(output);

    return 0;
}
