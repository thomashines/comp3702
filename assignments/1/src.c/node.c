#include "node.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Node *node_init() {
    Node *node = malloc(sizeof(Node));
    return node;
}

void node_free(Node *node) {
    free(node->to_nodes);
    free(node->to_node_costs);
    free(node);
}

void node_connect(Node *from_node, Node *to_node, float cost) {
    // Allocate more memory
    from_node->to_node_count++;
    from_node->to_nodes = realloc(from_node->to_nodes,
        from_node->to_node_count * sizeof(Node *));
    from_node->to_node_costs = realloc(from_node->to_node_costs,
        from_node->to_node_count * sizeof(float));

    // Add connection
    // printf("A %p\n", (void *) from_node->to_nodes);
    // printf("A %lu\n", from_node->to_node_count * sizeof(Node *));
    // printf("A %d\n", from_node->to_node_count);
    from_node->to_nodes[from_node->to_node_count - 1] = to_node;
    // printf("B\n");
    from_node->to_node_costs[from_node->to_node_count - 1] = cost;
    // printf("C\n");

    // Update search info
    if (cost < from_node->to_node_costs_min) {
        from_node->to_node_costs_min = cost;
    }
}

// Writes the string representations of node to the given output stream
void node_fprint(FILE *stream, Node *node) {
    fprintf(stream, "<%d ", node->index);

    // Total distance
    if (node->distance_total == INFINITY) {
        fprintf(stream, "(INF)>");
    } else {
        fprintf(stream, "(%.2f)>", node->distance_total);
    }
}

int node_route_fprint(FILE *stream, Node *node) {
    // Return [from route + "-"] + node index
    int depth = 0;

    if (node->best_from_node != NULL) {
        // Print previous nodes
        depth = node_route_fprint(stream, node->best_from_node) + 1;
        fprintf(stream, "-");
    }

    // Print this node
    fprintf(stream, "%d", node->index);

    return depth;
}

void node_route_costs_fprint(FILE *stream, Node *node) {
    // Return [from route + "-"] + node index

    if (node->best_from_node != NULL) {
        // Print previous nodes
        node_route_costs_fprint(stream, node->best_from_node);
        fprintf(stream, "-(%.2f)->", node->best_from_node_cost);
    }

    // Print this node
    fprintf(stream, "%d", node->index);
}
