#include "nodelist.h"

#include <stdio.h>
#include <stdlib.h>

// Empties a NodeList and frees all allocated memory
NodeList *nodelist_free(NodeList *nodelist) {
    if (nodelist != NULL) {
        nodelist_free(nodelist->nodelist);
        free(nodelist);
    }
    return NULL;
}

// Appends a Node to the end of a NodeList, returns the NodeList
NodeList *nodelist_push(NodeList *nodelist, Node *node) {
    if (nodelist == NULL) {
        // Allocate memory for this node list
        nodelist = (NodeList *) malloc(sizeof(NodeList));
        // Copy node value
        nodelist->node = node;
        // Set new tail to NULL
        nodelist->nodelist = NULL;
    } else {
        // Push node to tail
        nodelist->nodelist = nodelist_push(nodelist->nodelist, node);
    }
    return nodelist;
}

// Adds a Node to a NodeList maintaining ordering, returns the NodeList
NodeList *nodelist_push_sorted(NodeList *nodelist, Node *node) {
    if (nodelist == NULL ||
            (node->distance_total <= nodelist->node->distance_total)) {
        // Add node here
        NodeList *new_node_list = (NodeList *) malloc(sizeof(NodeList));
        new_node_list->node = node;
        // Set the old node list as the new tail
        new_node_list->nodelist = nodelist;
        // Return the new node list
        nodelist = new_node_list;
    } else if (node == nodelist->node) {
        // Node is already in list, don't duplicate it
    } else {
        // Push the new node to the tail
        nodelist->nodelist = nodelist_push_sorted(nodelist->nodelist,
            node);
    }
    return nodelist;
}

// Removes all instances of a Node from a NodeList, returns the NodeList
NodeList *nodelist_remove(NodeList *nodelist, Node *node) {
    if (nodelist != NULL) {
        if (nodelist->node == node) {
            // Remove this node
            // Get tail
            NodeList *oldTail = nodelist->nodelist;
            // Free memory allocated to this node
            free(nodelist);
            // Become the old tail
            nodelist = oldTail;
        } else {
            nodelist->nodelist = nodelist_remove(nodelist->nodelist, node);
        }
    }
    return nodelist;
}

// Sorts the given NodeList and returns the sorted NodeList
NodeList *nodelist_sort(NodeList *nodelist) {
    NodeList *sorted = NULL;
    // Until the original list is empty
    while (nodelist != NULL) {
        // Get the lowest node
        Node *lowest = nodelist_get_lowest(nodelist);
        // Remove it from the original list
        nodelist = nodelist_remove(nodelist, lowest);
        // Add it to the new list
        sorted = nodelist_push(sorted, lowest);
    }
    // Return the sorted list
    return sorted;
}

/* Removes the first Node from the NodeList, stores it in node and returns the
 * NodeList
 */
NodeList *nodelist_pop_first(NodeList *nodelist, Node **node) {
    if (nodelist != NULL) {
        // Copy this node
        *node = nodelist->node;
        // Remove this node
        // Get tail
        NodeList *oldTail = nodelist->nodelist;
        // Free memory allocated to this node
        free(nodelist);
        // Become the old tail
        nodelist = oldTail;
    }
    return nodelist;
}

/* Gets the Node at the specified index from the NodeList and copies it into
 * node
 */
Node *nodelist_get(NodeList *nodelist, int index) {
    if (nodelist == NULL) {
        printf("index out of bounds\n");
    }

    if (index <= 0) {
        // Return node
        return nodelist->node;
    }

    // Otherwise, get the node from the tail
    return nodelist_get(nodelist->nodelist, index - 1);
}

// Copies the highest Node in the NodeList into node
Node *nodelist_get_highest(NodeList *nodelist) {
    if (nodelist->nodelist != NULL) {
        // Get highest node from tail
        Node *highest = nodelist_get_highest(nodelist->nodelist);
        // Compare to this node
        if (nodelist->node->distance_total > highest->distance_total) {
            // This node is higher than its tail
            return nodelist->node;
        } else {
            // Tail has a higher node
            return highest;
        }
    }
    // One node, return it
    return nodelist->node;
}

// Copies the lowest Node in the NodeList into node
Node *nodelist_get_lowest(NodeList *nodelist) {
    if (nodelist->nodelist != NULL) {
        // Get lowest node from tail
        Node *lowest = nodelist_get_lowest(nodelist->nodelist);
        // Compare to this node
        if (nodelist->node->distance_total < lowest->distance_total) {
            // This node is lower than its tail
            return nodelist->node;
        } else {
            // Tail has a lower node
            return lowest;
        }
    }
    // One node, return it
    return nodelist->node;
}

// Returns the number of Nodes in a NodeList
int nodelist_count(NodeList *nodelist) {
    // printf("G\n");
    if (nodelist == NULL) {
        return 0;
    }
    return 1 + nodelist_count(nodelist->nodelist);
}

// Returns 1 if nodelist contains the Node
int nodelist_contains(NodeList *nodelist, Node *node) {
    if (nodelist == NULL) {
        return 0;
    }
    if (nodelist->node == node) {
        return 1;
    }
    return nodelist_contains(nodelist->nodelist, node);
}

/* Writes the string representations of the Nodes in nodelist to the given
 * output stream
 * Writes nodes up to the index in count, or all nodes if count < 0
 */
void nodelist_fprint(FILE *stream, NodeList *nodelist, int count) {
    if (count != 0 && nodelist != NULL) {
        // Print node
        node_fprint(stream, nodelist->node);
        if (count != 1 && nodelist->nodelist != NULL) {
            // Print tail
            fprintf(stream, ", ");
            nodelist_fprint(stream, nodelist->nodelist, count - 1);
        }
    }
    return;
}
