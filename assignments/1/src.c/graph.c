#include "graph.h"

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "node.h"
#include "nodelist.h"

Graph *graph_init(char *environment_file) {
    // Load file
    FILE *environment = fopen(environment_file, "r");

    // Read node count
    int node_count;
    fscanf(environment, "%d\n", &node_count);
    printf("%d nodes\n", node_count);

    // Allocate memory
    Graph *graph = malloc(sizeof(Graph));
    graph->node_count = node_count;
    graph->nodes = malloc(graph->node_count * sizeof(Node));

    // For each node
    for (int n = 0; n < graph->node_count; n++) {
        // Assign index
        graph->nodes[n].index = n + 1;
        // Clear A* info
        graph->nodes[n].to_node_costs_min = INFINITY;
        // Clear to nodes
        graph->nodes[n].to_node_count = 0;
        graph->nodes[n].to_nodes = NULL;
        graph->nodes[n].to_node_costs = NULL;
    }

    // Go line by line
    float cost;
    int from_node = 0;
    int to_node = 0;
    while (fscanf(environment, "%f", &cost) > 0) {
        if (cost > 0) {
            // printf("%d to %d for %f\n", from_node + 1, to_node + 1, cost);
            node_connect(
                &graph->nodes[from_node], &graph->nodes[to_node], cost);
        }

        // Increment to node
        to_node++;
        if (to_node == graph->node_count) {
            from_node++;
            to_node = 0;
        }
    }

    fclose(environment);

    return graph;
}

int graph_navigate(Graph *graph, int from, int to,
        float (*heuristic)(Node *node, int goal)) {
    // Clear state
    for (int i = 0; i < graph->node_count; i++) {
        graph->nodes[i].distance = INFINITY;
        graph->nodes[i].distance_total = INFINITY;
        graph->nodes[i].visited = 0;
        graph->nodes[i].best_from_node = NULL;
        graph->nodes[i].best_from_node_cost = INFINITY;
    }

    // Set from node's distance to 0
    graph->nodes[from - 1].distance = 0;
    graph->nodes[from - 1].distance_total = graph->nodes[from - 1].distance +
        heuristic(&graph->nodes[from - 1], to);

    // Make sorted set of fringe nodes
    NodeList *fringe_nodes = NULL;
    fringe_nodes = nodelist_push_sorted(fringe_nodes, &graph->nodes[from - 1]);

    // For at most every node
    int expanded = 0;
    while (nodelist_count(fringe_nodes) > 0) {
        // Get nearest fringe node
        Node *nearest;
        fringe_nodes = nodelist_pop_first(fringe_nodes, &nearest);

        // Check if it is the destination
        if (nearest->index == to) {
            break;
        }

        // Check if it hasn't been visited
        if (!nearest->visited) {
            // Visit it
            // printf("visiting ");
            // node_fprint(stdout, nearest);
            // printf("\n");
            nearest->visited = 1;
            expanded++;

            // For each of its neighbours
            for (int n = 0; n < nearest->to_node_count; n++) {
                // Get the neighbour
                Node *neighbour = nearest->to_nodes[n];

                // Check if it hasn't been visited
                if (!neighbour->visited) {
                    // Calculate new distance
                    float new_distance = nearest->distance +
                        nearest->to_node_costs[n];

                    // Compare to current distance
                    if (new_distance < neighbour->distance) {
                        // Copy across new route
                        neighbour->distance = new_distance;
                        neighbour->best_from_node = nearest;
                        neighbour->best_from_node_cost = nearest->to_node_costs[n];

                        // Update total distance with heuristic
                        neighbour->distance_total = neighbour->distance +
                            heuristic(neighbour, to);

                        // Push it to fringe set, sorted
                        // TODO: use something other than a linked list,
                        //       something with O(<n) time insert sorted
                        fringe_nodes = nodelist_push_sorted(fringe_nodes,
                            neighbour);
                    }
                }
            }
        }
    }

    return expanded;
}

float heuristic_uniform(Node *node, int goal) {
    return 0;
}

void graph_navigate_uniform(FILE *stream, Graph *graph, int from, int to) {
    // Do navigation
    int check = graph_navigate(graph, from, to, heuristic_uniform);

    // Print result
    printf("Uniform %4d->%4d for %6.2f visited %4d: ", from, to,
        graph->nodes[to - 1].distance, check);
    // node_route_costs_fprint(stdout, &graph->nodes[to - 1]);
    node_route_fprint(stdout, &graph->nodes[to - 1]);
    printf("\n");
    int depth = node_route_fprint(stream, &graph->nodes[to - 1]);
    fprintf(stream, " cost %.2f at depth %d\n",
        graph->nodes[to - 1].distance, depth);
}

float heuristic_astar(Node *node, int goal) {
    if (node->index == goal) {
        return 0;
    }
    return node->to_node_costs_min;
}

void graph_navigate_astar(FILE *stream, Graph *graph, int from, int to) {
    // Do navigation
    int check = graph_navigate(graph, from, to, heuristic_astar);

    // Print result
    printf("A*      %4d->%4d for %6.2f visited %4d: ", from, to,
        graph->nodes[to - 1].distance, check);
    // node_route_costs_fprint(stdout, &graph->nodes[to - 1]);
    node_route_fprint(stdout, &graph->nodes[to - 1]);
    printf("\n");
    int depth = node_route_fprint(stream, &graph->nodes[to - 1]);
    fprintf(stream, " cost %.2f at depth %d\n",
        graph->nodes[to - 1].distance, depth);
}
