#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1cm
\topmargin 1cm
\rightmargin 1cm
\bottommargin 2cm
\footskip 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
COMP3702 Assignment 3
\end_layout

\begin_layout Author
Thomas Hines
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

43174216
\end_layout

\begin_layout Section
Please define your MDP problem.
\end_layout

\begin_layout Standard
The problem specification gives the following parameters:
\end_layout

\begin_layout Standard
\begin_inset space \hfill{}
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="5">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Store size
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Types of items, 
\begin_inset Formula $t$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Inventory capacity, 
\begin_inset Formula $c$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Max purchases, 
\begin_inset Formula $p$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Max returns, 
\begin_inset Formula $r$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tiny
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Medium
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Large
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
10
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Mega
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
7
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Standard
The MDP problem is defined in the following ways: state space (
\begin_inset Formula $S$
\end_inset

), action space (
\begin_inset Formula $A$
\end_inset

), transition function (
\begin_inset Formula $T$
\end_inset

) and reward function (
\begin_inset Formula $R$
\end_inset

).
 I defined these as follows:
\end_layout

\begin_layout Subsection
State space, 
\begin_inset Formula $S$
\end_inset


\end_layout

\begin_layout Standard
The state space contains all the possible inventory states.
 These are the combinations with replacement of selecting 
\begin_inset Formula $c$
\end_inset

 of 
\begin_inset Formula $t+1$
\end_inset

 possible items, where the 
\begin_inset Formula $+1$
\end_inset

 allows for empty inventory slots.
 There are 
\begin_inset Formula $\begin{pmatrix}t+1+c-1\\
c
\end{pmatrix}$
\end_inset

 possible states.
\end_layout

\begin_layout Subsection
Action space, 
\begin_inset Formula $A$
\end_inset


\end_layout

\begin_layout Standard
The action space contains all the possible purchases and returns that can
 be made.
 Once again, the purchases are the combinations of selecting 
\begin_inset Formula $p$
\end_inset

 of 
\begin_inset Formula $t+1$
\end_inset

 items, and the returns are the combinations of selecting 
\begin_inset Formula $r$
\end_inset

 of 
\begin_inset Formula $t+1$
\end_inset

 items.
 There are 
\begin_inset Formula $\begin{pmatrix}t+1+p-1\\
p
\end{pmatrix}\times\begin{pmatrix}t+1+r-1\\
r
\end{pmatrix}$
\end_inset

 possible actions from any state (including impractical actions such as
 ordering 1 of item type 1 then returning 1 of item type 1 as well as ordering
 or returning more items that can added to or taken away from the current
 inventory).
\end_layout

\begin_layout Subsection
Transition function, 
\begin_inset Formula $T$
\end_inset


\end_layout

\begin_layout Standard
Each action results in a deterministic change of state, from the state before
 purchases and returns to the state after purchases and returns.
 After the action has been applied and the state has moved from 
\begin_inset Formula $s$
\end_inset

 to 
\begin_inset Formula $s'$
\end_inset

, the customer's requests come in, which are of an unknown stochastic value.
\end_layout

\begin_layout Standard
The customer requests are for any quantity in 
\begin_inset Formula $[0,c]$
\end_inset

 of each item type, meaning there are 
\begin_inset Formula $t^{c}$
\end_inset

 possible customer requests, or transitions.
 There is a probability associated with each possible transition:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
T(s',o)=P(o|s')
\]

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $o$
\end_inset

 is the transition or customer request and 
\begin_inset Formula $s'$
\end_inset

 is the state immediately before the customer request came in.
\end_layout

\begin_layout Standard
The probability is not dependent on the action that was just performed,
 though it is dependent on the state after the action was performed, 
\begin_inset Formula $s'$
\end_inset

.
\end_layout

\begin_layout Subsection
Reward function, 
\begin_inset Formula $R$
\end_inset


\end_layout

\begin_layout Standard
The rewards are applied in two stages:
\end_layout

\begin_layout Standard
Each action is associated with a reward: -50% of the price of each returned
 item as well as a constant loss if the order needs to be modified (i.e.
 if there is not enough inventory capacity to fit the purchases or not enough
 stock to fill the returns).
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R_{action}(s,a)
\]

\end_inset


\end_layout

\begin_layout Standard
Each customer request is associated with a reward: 75% of the item price
 of each sold item as well as -25% of the item price of each requested item
 that is out of stock.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
R_{transition}(s',o)
\]

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $s'$
\end_inset

 is the state after action is applied and 
\begin_inset Formula $o$
\end_inset

 is the customer requests.
\end_layout

\begin_layout Section
Please explain your algorithm for solving the problem.
\end_layout

\begin_layout Standard
My solution uses value iteration with synchronous updates to build a policy,
 
\begin_inset Formula $\pi(s)$
\end_inset

, to decide what action to take from every possible state.
\end_layout

\begin_layout Standard
First, a directed graph is built.
 Each vertex represents a possible inventory state.
 There are 2 types of edges between vertices: 
\series bold
action edges
\series default
 and 
\series bold
transition edges
\series default
.
\end_layout

\begin_layout Standard

\series bold
Action edges
\series default
 represent actions: they go from one state to another according to a specific
 combination of purchases and returns and have a reward for following them.
 There is one for every practical action that is available from each state.
 An action is impractical if there is another action that leads to the same
 state but with a higher associated reward.
\end_layout

\begin_layout Standard

\series bold
Transition edges
\series default
 represent customer requests and also have a reward for following them.
 There is one for every customer request for every inventory state.
 Each transition edge has an associated probability determined by the transition
 function.
\end_layout

\begin_layout Standard
\begin_inset space \hfill{}
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="4">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Store size
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Number of vertices
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Number of action edges
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Number of transition edges
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tiny
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
10
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
51
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
160
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
45
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
661
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3645
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Medium
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
129
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4290
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
94041
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Standard
Next, the actual value iteration process begins.
\end_layout

\begin_layout Standard
For every vertex, a value is assigned to every action edge, then the policy
 for the state associated with the vertex is set to the action with the
 highest value.
\end_layout

\begin_layout Standard
Initially, all action edge values are set to 0, and the policy is any possible
 action for any state.
\end_layout

\begin_layout Standard
To calculate the value of an action edge, first calculate the reward associated
 with the edge, 
\begin_inset Formula $R_{action}(s,a)$
\end_inset

, then add the expected value of the possible transitions.
 The total value is:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
V(s,a)=R_{action}(s,a)+\gamma\sum_{o}T(s',o)(R_{transition}(s',o)+V_{\pi}(s''))
\]

\end_inset


\end_layout

\begin_layout Standard
Where 
\begin_inset Formula $V(s,a)$
\end_inset

 is the value of the action edge, 
\begin_inset Formula $\gamma$
\end_inset

 is the discount factor, 
\begin_inset Formula $s'$
\end_inset

 is the state after action 
\begin_inset Formula $a$
\end_inset

 is applied, 
\begin_inset Formula $s''$
\end_inset

 is the state after the customer request 
\begin_inset Formula $o$
\end_inset

 is applied, 
\begin_inset Formula $V_{\pi}(s'')$
\end_inset

 is the value of state 
\begin_inset Formula $s''$
\end_inset

.
\end_layout

\begin_layout Standard
Finally, 
\begin_inset Formula $V_{\pi}(s)$
\end_inset

 is updated to be the max of 
\begin_inset Formula $V(s,a)$
\end_inset

 of each action edge connected to the vertex that represents 
\begin_inset Formula $s$
\end_inset

.
\end_layout

\begin_layout Standard
This process is repeated for every vertex for either 2 minutes or until
 the largest change in value between iterations is less than 
\begin_inset Formula $1E-3$
\end_inset

.
\end_layout

\begin_layout Standard
To determine the action, 
\begin_inset Formula $a$
\end_inset

, to take according to the policy from state, 
\begin_inset Formula $s$
\end_inset

: find the vertex that represents 
\begin_inset Formula $s$
\end_inset

, take the action from the action edge with the highest 
\begin_inset Formula $V(s,a)$
\end_inset

 value.
\end_layout

\begin_layout Section
Please analyse your algorithm’s accuracy and speed, via comparison study.
 For this purpose, at the very least, you need to compare your method against
 the base value iteration with synchronous update (as described in class)
 on the 3 tiny and small problems in the input example.
 If the algorithm you implement is the aforementioned basic value iteration,
 please explain how you would modify it to solve the harder problem, i.e.,
 medium and large.
\end_layout

\begin_layout Standard
My solution is the basic value iteration with synchronous updates.
 It is capable of reliably solving tiny, small and medium problems.
 Large and mega stores result in massive graphs that are impractical to
 store in memory or analyse in a reasonable time frame.
 This means that the solution would definitely need to be modified to solve
 the large problem.
\end_layout

\begin_layout Standard
I ran 3 different inputs for 3 different sizes and recorded the offline
 processing time as well as the final profit.
 The solution is an offline solution so the online steps were instantaneous.
\end_layout

\begin_layout Standard
\begin_inset space \hfill{}
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="10" columns="6">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Store
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Average
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tiny 1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1253 in 64ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1105 in 67ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+467 in 67ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1190 in 66ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1004 in 66ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tiny 2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+757 in 53ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+711 in 55ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+773 in 54ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+579 in 56ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+705 in 55ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Tiny 3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1917 in 63ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+2211 in 63ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+2451 in 61ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+2276 in 60ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+2214 in 62ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small 1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+724 in 120ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+302 in 129ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
-203 in 139ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1321 in 129ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+536 in 129ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small 2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+209 in 119ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+558 in 120ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+617 in 120ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+407 in 127ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+448 in 122ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Small 3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1201 in 128ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1207 in 133ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1672 in 147ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1795 in 129ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+1469 in 134ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Medium 1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+804 in 1158ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+483 in 1180ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+856 in 1162ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+658 in 1170ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+700 in 1168ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Medium 2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+806 in 2432ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+483 in 2467ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+544 in 2685ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+995 in 2669ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+707 in 2563ms
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Medium 3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+446 in 1155ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+118 in 1026ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+287 in 1013ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+190 in 1027ms
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
+260 in 4221ms
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Standard
One way to solve this is by splitting the problem into 2 smaller problems.
 The downside of this is that the value iteration method will not converge
 to the optimal solution, however optimality is not required, only profitability.
\end_layout

\begin_layout Standard
The large store can be split into 2 smaller stores:
\end_layout

\begin_layout Standard
\begin_inset space \hfill{}
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="5">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Store size
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Types of items, 
\begin_inset Formula $t$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Inventory capacity, 
\begin_inset Formula $c$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Max purchases, 
\begin_inset Formula $p$
\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Max returns, 
\begin_inset Formula $r$
\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Large part 1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Large part 2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Standard
Both sub problems are smaller than the medium store, which is easily solved
 in less than 5% of the time limit.
\end_layout

\end_body
\end_document
