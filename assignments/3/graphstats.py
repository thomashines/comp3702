#!/usr/bin/env python

from scipy.misc import comb

store_types = [
    # ["type", types, inventory, orders, returns],
    [  "tiny", 2,  3, 2, 1],
    [ "small", 2,  8, 3, 2],
    ["medium", 3,  8, 3, 2],
    [ "large", 5, 10, 4, 2],
    [  "mega", 7, 20, 5, 3],
]

if __name__ == "__main__":
    print("//   size, types, inventory, orders, returns, " +
        "states, actions, transitions")
    for name, T, M, O, F in store_types:
        states = int(comb((T + 1) + M - 1, M))
        orders = int(comb((T + 1) + O - 1, O))
        returns = int(comb((T + 1) + F - 1, F))
        transitions = (M + 1) ** T
        print("// %6s, %5d, %9d, %6d, %7d, %6d, %7d, %11d" % (
            name, T, M, O, F, states, orders * returns, transitions))
