package solver;

import java.util.Arrays;

public class Transition {
    // Graph structure
    public int[] request;
    public double reward;
    public double probability;
    public Vertex to;

    // Transition string representation
    @Override
    public String toString() {
        return "Transition{" + hashCode() + "}" + Arrays.toString(request) +
            "@" + reward;
    }
}
