package solver;

import java.util.Arrays;
import java.util.Collection;
import java.lang.Math;

public class Vertex implements Comparable<Vertex> {
    // Graph structure
    public int[] state;
    public Action[] actions;
    public Transition[] transitions;

    // Value iteration
    public Action bestAction;
    public double bestActionValue;
    public double adjustment;

    // Constructor
    public Vertex(int[] state) {
        // Copy state
        this.state = state.clone();
        // Value iteration
        bestActionValue = 0;
    }

    // Get state size
    public int getSize() {
        return state.length;
    }

    // Get state
    public int[] getState() {
        return state.clone();
    }

    // Set actions
    public void setActions(Collection<Action> actions) {
        this.actions = actions.toArray(new Action[actions.size()]);
        // System.out.printf("actions from %s: %d\n", this, this.actions.length);
    }

    // Set transitions
    public void setTransitions(Collection<Transition> transitions) {
        this.transitions = transitions.toArray(
            new Transition[transitions.size()]);
        // System.out.printf("transitions from %s: %d\n", this,
        //     this.transitions.length);
        // Sum probabilities
        // double sum = 0;
        // for (Transition transition : this.transitions) {
        //     sum += transition.probability;
        // }
        // System.out.printf("\ttotal probability %f\n", sum);
    }

    // Compare
    @Override
    public int compareTo(Vertex other) {
        return (int) Math.round(Math.signum(this.adjustment - other.adjustment));
    }

    // Vertex string representation
    @Override
    public String toString() {
        return "Vertex{" + hashCode() + "}" + Arrays.toString(state);
    }

    // Vertices are equal if they have the same state
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Vertex)) {
            return false;
        }
        Vertex otherVertex = (Vertex) other;
        if (otherVertex.getSize() != state.length) {
            return false;
        }
        int[] otherState = otherVertex.getState();
        for (int i = 0; i < state.length; i++) {
            if (otherState[i] != state[i]) {
                return false;
            }
        }
        return true;
    }

    // Hashcode is dependent solely on state
    private static final int[] hashPrimes = {23, 29, 31, 37, 41, 43, 47};
    @Override
    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < state.length; i++) {
            hash += state[i] * hashPrimes[i];
        }
        return hash;
    }
}
