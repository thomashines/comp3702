package solver;

import java.util.Arrays;

public class Action {
    // Graph structure
    public int[] order;
    public int[] ret;
    public double reward;
    public Vertex to;

    // Value iteration
    public double value;

    public Action() {
        value = 0;
    }

    // Action string representation
    @Override
    public String toString() {
        return "Action{" + hashCode() + "}" + Arrays.toString(order) +
            Arrays.toString(ret) + "@" + reward;
    }

    // Actions are equal if they have the same final state
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Action)) {
            return false;
        }
        Action otherAction = (Action) other;
        return otherAction.to.equals(to);
    }

    // Hashcode is dependent solely on final state
    @Override
    public int hashCode() {
        return to.hashCode();
    }
}
