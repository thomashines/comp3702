package solver;

import java.io.IOException;
import java.lang.Math;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.IntStream;

import problem.Matrix;
import problem.ProblemSpec;
import problem.Store;

public class MySolver implements OrderingAgent {

    // Problem information
    private ProblemSpec spec = new ProblemSpec();
    private Store store;
    private double discountFactor;
    private double penaltyFee;
    private int capacity;
    private int maxPurchase;
    private int maxReturns;
    private int maxTypes;
    private Double[] prices;
    private Matrix[] probabilities;

    // Graph structure
    private HashMap<Integer, Vertex> vertices;

    public MySolver(ProblemSpec spec) throws IOException {
        // Get problem information
        this.spec = spec;
        store = spec.getStore();
        discountFactor = spec.getDiscountFactor();
        penaltyFee = spec.getPenaltyFee();
        capacity = store.getCapacity();
        maxPurchase = store.getMaxPurchase();
        maxReturns = store.getMaxReturns();
        maxTypes = store.getMaxTypes();
        prices = spec.getPrices().toArray(new Double[maxTypes]);
        probabilities = spec.getProbabilities().toArray(new Matrix[maxTypes]);
    }

    private void generateCombinations(ArrayList<int[]> combinations, int[] base,
            int index, int n, int r) {
        // Get count of selected so far
        int sum = IntStream.of(base).sum();
        // Try selecting all possible quantities of this bin
        for (int select = 0; select <= n - sum; select++) {
            base[index] = select;
            // If last bin, add to combinations array
            if (index == r - 1) {
                // System.out.printf("add %s\n", Arrays.toString(base));
                combinations.add(base.clone());
            } else {
                // If not last bin, recurse to next bin
                generateCombinations(combinations, base.clone(), index + 1, n,
                    r);
            }
        }
    }

    private void generateTransitions(ArrayList<int[]> transitions, int[] base,
            int index, int n, int r) {
        // Try selecting all possible quantities of this bin
        for (int select = 0; select <= n; select++) {
            base[index] = select;
            // If last bin, add to transitions array
            if (index == r - 1) {
                // System.out.printf("add %s\n", Arrays.toString(base));
                transitions.add(base.clone());
            } else {
                // If not last bin, recurse to next bin
                generateTransitions(transitions, base.clone(), index + 1, n, r);
            }
        }
    }

    private int[] applyAction(int[] from, int[] order, int[] ret) {
        for (int i = 0; i < from.length; i++) {
            from[i] += order[i] - ret[i];
        }
        return from;
    }

    private boolean isValid(int[] state, int capacity) {
        int sum = 0;
        for (int i = 0; i < state.length; i++) {
            if (state[i] < 0) {
                return false;
            }
            sum += state[i];
        }
        return sum <= capacity;
    }

    private double actionReward(int[] state, int[] order, int[] ret,
            int capacity, Double[] prices) {
        double reward = 0;

        // Returns cost 50% of item price
        for (int i = 0; i < ret.length; i++) {
            reward -= ret[i] * prices[i] * 0.5;
        }

        // Actions that require orders or returns to be cut are not considered
        // so the penalty will never be applied

        return reward;
    }

    private double transitionReward(int[] state, int[] request,
            Double[] prices) {
        double reward = 0;

        for (int i = 0; i < state.length; i++) {
            if (request[i] > state[i]) {
                // Sales earn 75% of item price
                reward += state[i] * prices[i] * 0.75;
                // Missed opportunities cost 25% of item price
                reward -= (request[i] - state[i]) * prices[i] * 0.25;
            } else {
                // Sales earn 75% of item price
                reward += request[i] * prices[i] * 0.75;
            }
        }

        return reward;
    }

    private double transitionProbability(int[] state, int[] request,
            Matrix[] probabilities) {
        double probability = 1;

        for (int i = 0; i < request.length; i++) {
            probability *= probabilities[i].get(state[i], request[i]);
        }

        return probability;
    }

    private int[] applyTransition(int[] from, int[] request) {
        for (int i = 0; i < from.length; i++) {
            if (request[i] > from[i]) {
                from[i] = 0;
            } else {
                from[i] -= request[i];
            }
        }
        return from;
    }

    public void doOfflineComputation() {
        // Parameters
        // M: max number of items stocked in store
        // T: max number of different types of items stocked in store
        // O: max number of items that can be ordered
        // F: max number of items that can be returned

        // From spec: ignore week, assume store is eternal
        // State: (inventory)
        // Action: (orders, returns)
        // Transition: apply action, handle customer requests
        // Value function: value at a state is the maximum of (reward from
        //  action + expected (reward from customer requests + value of next
        //  state)) for each state

        // Build a graph
        // Nodes: one node for each possible state
        // Edges: 2 types of edges:
        //  Action edges: one edge for every possible and practical action
        //  Transition edges: one stochastic edge for every possible customer
        //   request

        // Graph stats
        //   size, types, inventory, orders, returns, states, actions, transitions
        //   tiny,     2,         3,      2,       1,     10,      18,          16
        //  small,     2,         8,      3,       2,     45,      60,          81
        // medium,     3,         8,      3,       2,    165,     200,         729
        //  large,     5,        10,      4,       2,   3003,    2646,      161051
        //   mega,     7,        20,      5,       3, 888030,   95040,  1801088541
        // large and mega do not look feasible to do global value iteration

        // Value iteration
        // For each state:
        //  Set value to
        //   Max for each action edge:
        //    Action reward plus
        //    Discount times
        //    Sum for each transition edge from action state:
        //     Transition probability * value of transition state

        // Start timing
        long startTime = System.currentTimeMillis();

        // Create graph
        vertices = new HashMap<Integer, Vertex>();

        // Zero state
        int[] zeroState = new int[maxTypes];

        // Generate combinations
        ArrayList<int[]> combinations = new ArrayList<int[]>();
        generateCombinations(combinations, zeroState.clone(), 0,
            capacity, maxTypes);
        System.out.printf("states %d\n", combinations.size());

        // Add vertices to graph
        for (int[] state : combinations) {
            Vertex vertex = new Vertex(state);
            vertices.put(vertex.hashCode(), vertex);
        }

        // Generate possible returns
        ArrayList<int[]> returns = new ArrayList<int[]>();
        generateCombinations(returns, zeroState.clone(), 0,
            maxReturns, maxTypes);
        System.out.printf("returns %d\n", returns.size());

        // Generate possible orders
        ArrayList<int[]> orders = new ArrayList<int[]>();
        generateCombinations(orders, zeroState.clone(), 0,
            maxPurchase, maxTypes);
        System.out.printf("orders %d\n", orders.size());

        // Generate possible requests
        ArrayList<int[]> requests = new ArrayList<int[]>();
        generateTransitions(requests, zeroState.clone(), 0,
            capacity, maxTypes);
        System.out.printf("requests %d\n", requests.size());

        // Add edges to graph
        long actionEdges = 0;
        long transitionEdges = 0;
        for (Vertex vertex : vertices.values()) {
            // Actions
            HashMap<Integer, Action> actions = new HashMap<Integer, Action>();
            // Orders
            for (int[] order : orders) {
                // Check if possible
                int[] postOrder = applyAction(vertex.getState(), order,
                    zeroState);
                if (isValid(postOrder, capacity)) {
                    // Returns
                    for (int[] ret : returns) {
                        // Check if possible
                        int[] postReturn = applyAction(vertex.getState(), order,
                            ret);
                        if (isValid(postReturn, capacity)) {
                            // Make action
                            Action action = new Action();
                            action.order = order.clone();
                            action.ret = ret.clone();
                            action.reward = actionReward(vertex.getState(),
                                order, ret, capacity, prices);
                            action.to = new Vertex(postReturn);
                            action.to = vertices.get(action.to.hashCode());

                            // Check if exists in actions
                            if (actions.containsKey(action.hashCode())) {
                                // Compare rewards
                                if (action.reward > actions.get(
                                        action.hashCode()).reward) {
                                    // Replace action with new action
                                    actions.replace(action.hashCode(), action);
                                }
                            } else {
                                // Add new action
                                actions.put(action.hashCode(), action);
                                actionEdges++;
                            }
                        }
                    }
                }
            }

            // Add actions to vertex
            vertex.setActions(actions.values());

            // Transitions
            ArrayDeque<Transition> transitions = new ArrayDeque<Transition>();
            for (int[] request : requests) {
                Transition transition = new Transition();
                transition.request = request.clone();
                transition.reward = transitionReward(vertex.getState(), request,
                    prices);
                transition.probability = transitionProbability(
                    vertex.getState(), request, probabilities);
                transition.to = new Vertex(applyTransition(vertex.getState(),
                    request));
                transition.to = vertices.get(transition.to.hashCode());
                transitions.add(transition);
                transitionEdges++;
            }

            // Add transitions to vertex
            vertex.setTransitions(transitions);
        }

        // Print graph stats
        System.out.printf("vertices %d\n", vertices.size());
        System.out.printf("action edges %d\n", actionEdges);
        System.out.printf("transition edges %d\n", transitionEdges);

        // Make priority queue
        // PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();

        // Clear state
        for (Vertex vertex : vertices.values()) {
            vertex.bestAction = vertex.actions[0];
            vertex.bestActionValue = vertex.actions[0].value;
            vertex.adjustment = 1;
            // vertexQueue.add(vertex);
        }

        // Convergence tolerance and timeout
        double tolerance = 1E-3;
        long timeout = 120*1000;

        // Do value iteration with priority queue
        // while (vertexQueue.peek().adjustment > tolerance) {
        //     Vertex vertex = vertexQueue.poll();
        // }

        // Do value iteration with tolerance
        int iterations = 0;
        double maxAdjustment = tolerance + 1;
        while (maxAdjustment > tolerance) {
            iterations++;
            maxAdjustment = 0;
            for (Vertex vertex : vertices.values()) {
                // Start with the first action in the list
                double originalValue = vertex.bestActionValue;
                vertex.bestAction = vertex.actions[0];
                vertex.bestActionValue = vertex.actions[0].value;

                // Calculate all action values
                for (Action action : vertex.actions) {
                    // Update action value
                    action.value = action.reward;
                    for (Transition transition : action.to.transitions) {
                        action.value += discountFactor *
                            transition.probability * (
                                transition.reward +
                                transition.to.bestActionValue);
                    }
                    // System.out.printf("action.value %f\n", action.value);

                    // Check if best action
                    if (action.value > vertex.bestActionValue) {
                        vertex.bestAction = action;
                        vertex.bestActionValue = action.value;
                    }
                }

                // Get change
                vertex.adjustment = Math.abs(vertex.bestActionValue - originalValue);
                if (vertex.adjustment > maxAdjustment) {
                    maxAdjustment = vertex.adjustment;
                }
            }

            // Check timeout
            long endTime = System.currentTimeMillis();
            if (endTime - startTime > timeout) {
                break;
            }
        }
        System.out.printf("Value iterations %d\n", iterations);
        System.err.printf("Duration %dms\n",
            System.currentTimeMillis() - startTime);

        // Print out policy
        for (int[] state : combinations) {
            Vertex vertex = new Vertex(state);
            vertex = vertices.get(vertex.hashCode());
            int[] action = new int[maxTypes];
            for (int t = 0; t < maxTypes; t++) {
                action[t] =
                    vertex.bestAction.order[t] - vertex.bestAction.ret[t];
            }
            System.err.printf("State: %s Policy: %s\n",
                Arrays.toString(vertex.state),
                Arrays.toString(action));
        }
    }

    public List<Integer> generateStockOrder(List<Integer> stockInventory,
            int numWeeksLeft) {
        // Make vertex for state
        int[] state = new int[maxTypes];
        for (int i = 0; i < maxTypes; i++) {
            state[i] = stockInventory.get(i);
        }
        Vertex vertex = new Vertex(state);

        // Get vertex from graph
        vertex = vertices.get(vertex.hashCode());
        System.out.printf("at %s\n", vertex);
        System.out.printf("do %s\n", vertex.bestAction);

        // Compile order
        List<Integer> order = new ArrayList<Integer>(maxTypes);
        for (int i = 0; i < maxTypes; i++) {
            order.add(vertex.bestAction.order[i] - vertex.bestAction.ret[i]);
            // order.add(1);
        }

        // Return order
        return order;
    }

}
