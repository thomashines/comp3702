#!/usr/bin/env gnuplot

# Output
set terminal pngcairo
set output "xyscatter.png"

# Key
set key off

# Labels
set title "(x, y) of sampled configs"
set xlabel "x"
set ylabel "y"

# Plot
set style fill transparent solid 0.2 noborder
set style circle radius 0.0035
plot "xyscatter.csv" using 1:2 with circles lc rgb "blue"
