#include "collision.hpp"
#include "util.hpp"

// line-rect algorithms: https://en.wikipedia.org/wiki/Line_clipping
bool col_line_rect(double ax0, double ay0, double ax1, double ay1,
                   double bx0, double by0, double bx1, double by1) {
    // Check bounds
    if (!COL_RECT_RECT(MIN(ax0, ax1), MIN(ay0, ay1),
                       MAX(ax0, ax1), MAX(ay0, ay1),
                       bx0, by0, bx1, by1)) {
        return false;
    }

    // Check ends
    if (COL_POINT_RECT(ax0, ay0, bx0, by0, bx1, by1) ||
        COL_POINT_RECT(ax1, ay1, bx0, by0, bx1, by1)) {
        return true;
    }

    // Check intersects
    return col_line_line(ax0, ay0, ax1, ay1, bx0, by0, bx1, by0) || // Bottom
           col_line_line(ax0, ay0, ax1, ay1, bx0, by1, bx1, by1) || // Top
           col_line_line(ax0, ay0, ax1, ay1, bx0, by0, bx0, by1) || // Left
           col_line_line(ax0, ay0, ax1, ay1, bx1, by0, bx1, by1);   // Right
}

// line-line algorithms: stackoverflow
bool col_line_line(double ax0, double ay0, double ax1, double ay1,
                   double bx0, double by0, double bx1, double by1) {
    // Check bounds
    if (!COL_RECT_RECT(MIN(ax0, ax1), MIN(ay0, ay1),
                       MAX(ax0, ax1), MAX(ay0, ay1),
                       MIN(bx0, bx1), MIN(by0, by1),
                       MAX(bx0, bx1), MAX(by0, by1))) {
       return false;
    }

    double den = ((by1-by0)*(ax1-ax0)-(bx1-bx0)*(ay1-ay0));
    double num1 = ((bx1 - bx0)*(ay0-by0) - (by1- by0)*(ax0-bx0));
    double num2 = ((ax1-ax0)*(ay0-by0)-(ay1-ay0)*(ax0-bx0));
    double u1 = num1/den;
    double u2 = num2/den;
    // std::cout << u1 << ":" << u2 << std::endl;
    if (den == 0 && num1  == 0 && num2 == 0)
        /* The two lines are coincidents */
        return false;
    if (den == 0)
        /* The two lines are parallel */
        return false;
    if (u1 < 0 || u1 > 1 || u2 < 0 || u2 > 1)
        /* Lines do not collide */
        return false;
    /* Lines DO collide */
    return true;
}
