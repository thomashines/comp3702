#ifndef UTIL_HPP
#define UTIL_HPP

#include <string>
#include <valarray>

using namespace std;

// Pi
#define PI 3.14159265359

// Min/max
#define MIN(a,b) ((a < b) ? a : b)
#define MAX(a,b) ((a > b) ? a : b)

// Max double value
extern double double_inf;

// Put file contents into string
string file_to_string(string file);

// Get a string representation of a double valarray
string valarray_to_string(valarray<double> va);

// Returns a random double, for use in valarray::apply
double rand_double(double x);

// Returns square root of a double, for use in valarray::apply
double sqrt_double(double x);

// Returns square of a double, for use in valarray::apply
double sq_double(double x);

#endif
