#ifndef VERTEX_HPP
#define VERTEX_HPP

using namespace std;

// Vertexette
class Vertexette {
    public:
        int index;
        // Vertex *vertex;
        double cost;
        Vertexette(int index, double cost): index(index), cost(cost) { }
};

bool operator>(const Vertexette &lhs, const Vertexette &rhs);

bool operator<(const Vertexette &lhs, const Vertexette &rhs);

ostream& operator<<(ostream &stream, const Vertexette &vertexette);

// Vertex
class Vertex {
    public:
        // Position
        valarray<double> position;
        valarray<double> normalised;

        // Graph structure
        int index;
        int edge_count;
        vector<Vertexette> edges;

        // Search details
        bool visited;
        double cost_f; // sort metric, f = g + h
        double cost_g; // actual cost from initial
        double cost_h; // heuristic cost to goal

        // Route details
        Vertex *route_from;
        double route_from_cost;

        Vertex(void);
        void print_position(ostream &stream);
        void connect_to(int index, double cost);
        double dist_to_normalised(Vertex *vertex);
        string route_string(int *depth);
};

ostream& operator<<(ostream &stream, const Vertex &vertex);

#endif
