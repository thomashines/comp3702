#include <cmath>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <queue>
#include <sstream>
#include <streambuf>
#include <string>
#include <valarray>
#include <vector>

using namespace std;

// Modules
#include "util.hpp"
#include "collision.hpp"
#include "vertex.hpp"
#include "obstacle.hpp"
#include "navigator.hpp"

// Main
int main(int argc, char **argv) {
    // Check arguments
    if (argc != 3) {
        cout << "./a2-3702-43174216 <query file> <output file>";
        cout << endl;
    }

    Navigator navigator(argv[1], argv[2]);

    return 0;
}
