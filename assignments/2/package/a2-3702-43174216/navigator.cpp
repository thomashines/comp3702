#include <chrono>
#include <fstream>
#include <iostream>
#include <queue>
#include <random>
#include <sstream>
#include <string>
#include <valarray>
#include <vector>

#include "collision.hpp"
#include "config.hpp"
#include "navigator.hpp"
#include "obstacle.hpp"
#include "util.hpp"
#include "vertex.hpp"

using namespace std;

Navigator::Navigator(char *query_file, char *out_file) {
    // Set up clock
    clock_t before, after;
    before = clock();
    ms_startup = 0;
    ms_querying = 0;
    ms_graphing = 0;
    ms_searching = 0;
    ms_is_visible = 0;
    ms_is_valid = 0;
    ms_valarray = 0;

    // Load query file into string
    string query_string = file_to_string(query_file);
    stringstream query_stream(query_string);
    cout << query_string;

    // Get arm type
    string arm_type;
    getline(query_stream, arm_type);
    gripper = (arm_type.find("noGripper") == string::npos);

    // Get initial config
    string line;
    getline(query_stream, line);
    stringstream initial_stream(line);
    vector<double> initial_vector;
    double c;
    string c_string;
    while (getline(initial_stream, c_string, ' ')) {
        stringstream c_stream(c_string);
        c_stream >> c;
        initial_vector.push_back(c);
    }
    valarray<double> initial = valarray<double>(initial_vector.data(),
        initial_vector.size());

    // Get goal config
    getline(query_stream, line);
    stringstream goal_stream(line);
    vector<double> goal_vector;
    while (getline(goal_stream, c_string, ' ')) {
        stringstream c_stream(c_string);
        c_stream >> c;
        goal_vector.push_back(c);
    }
    valarray<double> goal = valarray<double>(goal_vector.data(),
        goal_vector.size());

    // Get dimension count
    dimensions = initial.size();

    // Set limits
    min = valarray<double>(dimensions);
    max = valarray<double>(dimensions);

    // Chair limits
    min[slice(0, 2, 1)] = DIST_MIN;
    max[slice(0, 2, 1)] = DIST_MAX;

    if (gripper) {
        joints = dimensions - 6;
        joints_end = dimensions - 4;

        // Gripper limits
        min[slice(joints_end, 4, 1)] = GRIP_MIN;
        max[slice(joints_end, 4, 1)] = GRIP_MAX;

        // Arm vector
        links = vector<Obstacle>(joints + 4);
    } else {
        joints = dimensions - 2;
        joints_end = dimensions;

        // Arm vector
        links = vector<Obstacle>(joints);
    }

    // Joint limits
    min[slice(2, joints, 1)] = ANGLE_MIN;
    max[slice(2, joints, 1)] = ANGLE_MAX;

    // Get obstacle count
    int obstacle_count;
    query_stream >> obstacle_count;

    // Get obstacles
    obstacles = vector<Obstacle>(obstacle_count + 1, Obstacle(0, 0, 0, 0));
    double x0, y0, x1, y1;
    for (int o = 0; o < obstacle_count; o++) {
        query_stream >> x0;
        query_stream >> y1;
        query_stream >> x1;
        query_stream >> y0;
        obstacles[o] = Obstacle(MIN(x0, x1), MIN(y0, y1), MAX(x0, x1), MAX(y0, y1));
    }

    // Get corridors
    cout << "corridors" << endl;
    for (vector<Obstacle>::iterator c0 = obstacles.begin(),
            end = obstacles.end() - 1; c0 < end; ++c0) {
        for (vector<Obstacle>::iterator c1 = c0 + 1; c1 < end; ++c1) {
            // Check for gaps
            bool horizontal_gap = MAX(c0->y0, c1->y0) > MIN(c0->y1, c1->y1);
            bool vertical_gap = MAX(c0->x0, c1->x0) > MIN(c0->x1, c1->x1);

            // Get corridor rectangle
            float x0, y0, x1, y1, xc, yc;
            if (horizontal_gap && vertical_gap) {
                // Obstacles are diagonal from each other, no corridor
                continue;
            } else if (horizontal_gap) {
                // Obstacles are one above the other
                // cout << "\thorizontal_gap" << endl;
                x0 = MIN(c0->x0, c1->x0);
                y0 = MIN(c0->y1, c1->y1);
                x1 = MAX(c0->x1, c1->x1);
                y1 = MAX(c0->y0, c1->y0);
            } else if (vertical_gap) {
                // Obstacles are one beside the other
                // cout << "\tvertical_gap" << endl;
                x0 = MIN(c0->x1, c1->x1);
                y0 = MIN(c0->y0, c1->y0);
                x1 = MAX(c0->x0, c1->x0);
                y1 = MAX(c0->y1, c1->y1);
            } else {
                // Obstacles are in collision, no corridor
                continue;
            }

            // Check if corridor collides with any obstacles
            bool collides = false;
            xc = (x0 + x1) / 2.0;
            yc = (y0 + y1) / 2.0;
            for (vector<Obstacle>::iterator c2 = obstacles.begin(); c2 < end;
                    ++c2) {
                if (COL_POINT_RECT(xc, yc, c2->x0, c2->y0, c2->x1, c2->y1)) {
                    collides = true;
                }
            }
            if (collides) {
                // Not really a corridor
                continue;
            }

            // Add to corridor vectors
            corridor_x.push_back(xc);
            corridor_w_x.push_back((x1 - x0) / 4.0);
            corridor_y.push_back(yc);
            corridor_w_y.push_back((y1 - y0) / 4.0);

            // Print corridor details
            cout << "\tc_x " << corridor_x.back();
            cout << " +- " << corridor_w_x.back() << endl;
            cout << "\tc_y " << corridor_y.back();
            cout << " +- " << corridor_w_y.back() << endl;
        }
    }
    cout << endl;

    // Add chair as obstacle
    chair_obstacle = &obstacles[obstacle_count];
    *chair_obstacle = Obstacle(0, 0, CHAIR_SIDE, CHAIR_SIDE);

    // Seed rand
    // seed = 0;
    seed = chrono::system_clock::now().time_since_epoch().count();
    generator = default_random_engine(seed);
    srand(seed);

    // Properties
    rand_min = min;
    rand_min[0] += CHAIR_SIDE / 2;
    rand_min[1] += CHAIR_SIDE / 2;
    rand_max = max;
    rand_max[0] -= CHAIR_SIDE / 2;
    rand_max[1] -= CHAIR_SIDE / 2;
    rand_range = rand_max - rand_min;

    // Normalise primitive steps to 1 for distance calculation
    normaliser = valarray<double>(dimensions);
    normaliser[slice(0, 2, 1)] = DIST_PRIM;
    normaliser[slice(2, joints, 1)] = ANGLE_PRIM;
    if (gripper) {
        normaliser[slice(joints_end, 4, 1)] = DIST_PRIM;
    }

    // Open sampled configs file
    samples_stream.open("xyscatter.csv");

    // Open output file
    ofstream out_stream;
    out_stream.open(out_file);
    out_stream.setf(ios::fixed, ios::floatfield);
    out_stream.precision(2);

    // Time startup
    after = clock();
    ms_startup = 1000 * (after - before) / (double) CLOCKS_PER_SEC;

    // Start timing
    before = clock();

    // Do query
    prm_astar(&initial, &goal);

    // Stop timing
    after = clock();
    ms_querying = 1000 * (after - before) / (double) CLOCKS_PER_SEC;

    // Print route
    int depth = 0;
    cout << "route " << vertices[1].route_string(&depth) << endl;
    int steps = 0;
    string motion = motion_string(&vertices[1], &steps);
    out_stream << (steps - 1) << endl;
    out_stream << motion;

    // Print result

    // Print times
    cout << "ms_startup " << ms_startup << "ms" << endl;
    cout << "ms_querying " << ms_querying << "ms" << endl;
    cout << "\tms_graphing " << ms_graphing << "ms" << endl;
    // cout << "\t\tms_is_visible " << ms_is_visible << "ms" << endl;
    // cout << "\t\t\tms_valarray " << ms_valarray << "ms" << endl;
    // cout << "\t\t\tms_is_valid " << ms_is_valid << "ms" << endl;
    cout << "\tms_searching " << ms_searching << "ms" << endl;
    samples_stream << "# ms_querying = " << ms_querying << ";" << endl;
}

bool Navigator::is_valid(valarray<double> *config) {
    // clock_t before = clock();
    // Reasons to be invalid:
    //  Joint angles or gripper lengths outside limits
    //   not necessary, sample within limits
    //  Any part of robot outside of workspace
    //   chair-workspace: point-rectangle (within)
    //    not necessary, sample within limits
    //   arm-workspace: point-rectangle (within)   ** 2
    //  Robot collides with itself
    //   arm-chair: line-rectangle                 ** 4
    //   arm-arm: line-line                        ** 3
    //  Robot collides with obstacle
    //   chair-obstacle: rectangle-rectangle       ** 1
    //   arm-obstacle: line-rectangle              ** 5

    // Move the chair obstacle to the chair
    chair_obstacle->x0 = (*config)[0] - CHAIR_SIDE / 2;
    chair_obstacle->y0 = (*config)[1] - CHAIR_SIDE / 2;
    chair_obstacle->x1 = (*config)[0] + CHAIR_SIDE / 2;
    chair_obstacle->y1 = (*config)[1] + CHAIR_SIDE / 2;

    // Check collisions

    // 1: chair-obstacle
    for(vector<Obstacle>::iterator it = obstacles.begin(),
            end = obstacles.end() - 1; it < end; ++it) {
        if (COL_RECT_RECT(chair_obstacle->x0,
                          chair_obstacle->y0,
                          chair_obstacle->x1,
                          chair_obstacle->y1,
                          it->x0, it->y0, it->x1, it->y1)) {
            // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
            return false;
        }
    }

    if (joints >= 1) {
        // Build arm
        double angle = 0;
        double x0 = (*config)[0], y0 = (*config)[1];
        double x1 = x0, y1 = y0;
        for (int j = 0; j < joints; j++) {
            // Remember the last joint position
            x0 = x1;
            y0 = y1;

            // Get next joint position
            angle += (*config)[j + 2];
            x1 += LINK_LENGTH * cos(angle);
            y1 += LINK_LENGTH * sin(angle);

            // Add link to arm
            links[j] = Obstacle(x0, y0, x1, y1);
        }

        if (gripper) {
            // Build gripper
            // u1
            angle += PI / 2;
            double u1x = x1 + (*config)[joints_end + 0] * cos(angle);
            double u1y = y1 + (*config)[joints_end + 0] * sin(angle);
            // l1
            double l1x = x1 - (*config)[joints_end + 2] * cos(angle);
            double l1y = y1 - (*config)[joints_end + 2] * sin(angle);
            // u2
            angle -= PI / 2;
            double u2x = u1x + (*config)[joints_end + 1] * cos(angle);
            double u2y = u1y + (*config)[joints_end + 1] * sin(angle);
            // l2
            double l2x = l1x + (*config)[joints_end + 3] * cos(angle);
            double l2y = l1y + (*config)[joints_end + 3] * sin(angle);

            // Add to arm
            links[joints + 0] = Obstacle( x1,  y1, u1x, u1y); // u1
            links[joints + 1] = Obstacle(u1x, u1y, u2x, u2y); // u2
            links[joints + 2] = Obstacle( x1,  y1, l1x, l1y); // l1
            links[joints + 3] = Obstacle(l1x, l1y, l2x, l2y); // l2
        }

        // Check back on arm
        double minx = (*config)[0], miny = (*config)[1];
        double maxx = minx, maxy = miny;
        for(vector<Obstacle>::iterator it = links.begin(),
                end = links.end(); it != end; ++it) {
            // double
            // Get joint end
            // x1 = it->x1;
            // y1 = it->y1;

            // 2: arm-workspace
            if (!COL_POINT_RECT(it->x0, it->y0, min[0], min[1], max[0], max[1]) ||
                !COL_POINT_RECT(it->x1, it->y1, min[0], min[1], max[0], max[1])) {
                // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
                return false;
            }

            // Update arm bounds
            x0 = MIN(it->x0, it->x1);
            y0 = MIN(it->y0, it->y1);
            x1 = MAX(it->x0, it->x1);
            y1 = MAX(it->y0, it->y1);
            if (x0 < minx) {
                minx = x0;
            } else if (x1 > maxx) {
                maxx = x1;
            }
            if (y0 < miny) {
                miny = y0;
            } else if (y1 > maxy) {
                maxy = y1;
            }
            // if (x1 < minx) {
            //     minx = x1;
            // } else if (x1 > maxx) {
            //     maxx = x1;
            // }
            // if (y1 < miny) {
            //     miny = y1;
            // } else if (y1 > maxy) {
            //     maxy = y1;
            // }
        }

        if (joints >= 2) {
            if (joints >= 3) {
                // 3:arm-arm
                for(vector<Obstacle>::iterator it0 = links.begin(),
                        end0 = links.end() - (gripper ? 6 : 2),
                        end1 = links.end(); it0 < end0; ++it0) {
                    for(vector<Obstacle>::iterator it1 = it0 + 2; it1 < end1; ++it1) {
                        if (col_line_line(it0->x0, it0->y0, it0->x1, it0->y1,
                                          it1->x0, it1->y0, it1->x1, it1->y1)) {
                            // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
                            return false;
                        }
                    }
                }
            }

            // 4: arm-chair
            for(vector<Obstacle>::iterator it = links.begin() + 1,
                    end = links.end(); it != end; ++it) {
                if (col_line_rect(it->x0, it->y0, it->x1, it->y1,
                                  chair_obstacle->x0,
                                  chair_obstacle->y0,
                                  chair_obstacle->x1,
                                  chair_obstacle->y1)) {
                    // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
                    return false;
                }
            }
        }

        // for(vector<Obstacle>::iterator itl = links.begin(),
        //         end = links.end(); itl != end; ++itl) {
        //     if (MIN(itl->x0, itl->x1) < minx) {
        //         cout << "minx is wrong" << endl;
        //     }
        //     if (MIN(itl->y0, itl->y1) < miny) {
        //         cout << "miny is wrong" << endl;
        //     }
        //     if (MAX(itl->x0, itl->x1) > maxx) {
        //         cout << "maxx is wrong" << endl;
        //     }
        //     if (MAX(itl->y0, itl->y1) > maxy) {
        //         cout << "maxy is wrong" << endl;
        //     }
        // }

        // 5: arm-obstacle
        for(vector<Obstacle>::iterator ito = obstacles.begin(),
                end = obstacles.end() - 1; ito < end; ++ito) {
            if (COL_RECT_RECT(minx, miny, maxx, maxy,
                              ito->x0, ito->y0, ito->x1, ito->y1)) {
                for(vector<Obstacle>::iterator itl = links.begin(),
                        end = links.end(); itl != end; ++itl) {
                    if (col_line_rect(itl->x0, itl->y0, itl->x1, itl->y1,
                                      ito->x0, ito->y0, ito->x1, ito->y1)) {
                        // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
                        return false;
                    }
                }
            }
        }
    }

    // ms_is_valid += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
    return true;
}

bool Navigator::is_visible(Vertex *from, Vertex *to) {
    // clock_t before = clock();

    // Get difference between configs
    // clock_t before_valarray = clock();
    valarray<double> diff = to->position - from->position;
    // ms_valarray += 1000 * (clock() - before_valarray) / (double) CLOCKS_PER_SEC;

    // Check configs along path
    // cout << "dist " << from->dist_to_normalised(to) << endl;
    // int validations = 0;
    double primitive_steps = from->dist_to_normalised(to) + 1;
    // cout << "primitive_steps " << primitive_steps - 1 << endl;
    // int checks = 0;
    double levels = ceil(log2(from->dist_to_normalised(to)));
    valarray<double> step_size = diff / primitive_steps;
    double steps_between;
    for (double l = 0; l < levels; l++) {
        steps_between = pow(2.0, levels - l);
        // before_valarray = clock();
        valarray<double> check_at = from->position + (steps_between / 2.0) * step_size;
        // ms_valarray += 1000 * (clock() - before_valarray) / (double) CLOCKS_PER_SEC;
        for (double s = steps_between / 2.0; s < primitive_steps; s += steps_between) {
            // checks++;
            if (!is_valid(&check_at)) {
                // ms_is_visible += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
                // cout << "validations " << validations << endl;
                return false;
            }
            // before_valarray = clock();
            check_at += steps_between * step_size;
            // ms_valarray += 1000 * (clock() - before_valarray) / (double) CLOCKS_PER_SEC;
        }
    }
    // cout << "checks " << checks << endl;

    // ms_is_visible += 1000 * (clock() - before) / (double) CLOCKS_PER_SEC;
    return true;
}

string Navigator::motion_string(Vertex *to, int *steps) {
    string motion;
    if (to->route_from) {
        // Get motion to here
        motion = motion_string(to->route_from, steps);

        // Append motion from here
        double dist = to->dist_to_normalised(to->route_from);
        double this_steps = ceil(dist) + 1;
        // cout << "this_steps " << this_steps << endl;
        valarray<double> start = to->route_from->position;
        valarray<double> diff = to->position - start;
        int s;
        for (s = 0; s <= this_steps; s++) {
            valarray<double> step = start + (s / this_steps) * diff;
            motion += valarray_to_string(step) + "\n";
        }
        *steps += s;
    }

    return motion;
}

void Navigator::prm_astar(valarray<double> *initial, valarray<double> *goal) {
    // Edges
    int edges = 0;

    // Start by adding the initial and goal configs as vertices
    vertices.push_back(Vertex());
    vertices.push_back(Vertex());

    Vertex *initial_vx = &vertices[0];
    initial_vx->index = 0;
    initial_vx->position = *initial;
    initial_vx->normalised = *initial / normaliser;

    Vertex *goal_vx = &vertices[1];
    goal_vx->index = 1;
    goal_vx->position = *goal;
    goal_vx->normalised = *goal / normaliser;

    // Check for a straight path
    double dist = initial_vx->dist_to_normalised(goal_vx);
    cout << "direct steps " << dist << endl;
    // Try to connect
    if (is_visible(initial_vx, goal_vx)) {
        cout << "goal visible from initial" << endl;
        initial_vx->connect_to(goal_vx->index, dist);
        goal_vx->connect_to(initial_vx->index, dist);
        edges++;
    }

    bool corridor_config = false;
    int corridor_configs = 0;
    int corridor_configs_invalid_ws = 0;
    int corridor_configs_invalid_ob = 0;
    int other_configs = 0;
    int other_configs_invalid = 0;

    // Timing
    clock_t before, after;

    // Until a path has been found
    bool found_goal = false;
    while (!found_goal) {
        // Add more vertices
        int new_vertices = vertices.size() * 0.5;
        before = clock();
        while (new_vertices > 0) {
            // Get a random config
            valarray<double> config(dimensions);
            config = config.apply(rand_double);
            config *= rand_range;
            config += rand_min;

            // Check near a corridor sometimes
            corridor_config = false;
            if (corridor_x.size() > 0 && ((rand() % 100) < CORRIDOR_PERCENT)) {
                corridor_config = true;
                corridor_configs++;

                // Get a random corridor
                int corridor = rand() % corridor_x.size();
                // cout << "c " << corridor << endl;

                // Set chair position using a normal distribution
                norm_dist_x = normal_distribution<double>(
                    corridor_x[corridor], corridor_w_x[corridor]);
                norm_dist_y = normal_distribution<double>(
                    corridor_y[corridor], corridor_w_y[corridor]);

                // Get a random chair position
                config[0] = norm_dist_x(generator);
                config[1] = norm_dist_y(generator);

                // Check chair position is in bounds
                if (config[0] < rand_min[0] || config[0] > rand_max[0] ||
                    config[1] < rand_min[1] || config[1] > rand_max[1]) {
                    corridor_configs_invalid_ws++;
                    continue;
                }
            } else {
                other_configs++;
            }

            // Check if it is valid
            if (is_valid(&config)) {
                samples_stream << config[0] << ", " << config[1] << endl;
                // Add it to the vertex vector
                vertices.push_back(Vertex());
                new_vertices--;
                Vertex *new_vertex = &vertices.back();
                new_vertex->index = vertices.size() - 1;
                new_vertex->position = config;
                new_vertex->normalised = config / normaliser;

                // Try to make edges to all nearby vectors
                int vertex_count = new_vertex->index;
                Vertex *connect;
                double dist;
                int tried_edges = 0, new_edges = 0;
                int edges_to_try = vertex_count * TRY_EDGES_RATIO;
                while (tried_edges < edges_to_try && new_edges != NEW_EDGES) {
                    #ifdef TRY_RANDOM_EDGES
                        if (tried_edges > 2) {
                            connect = &vertices[rand() % vertex_count];
                            bool connected = false;
                            for (vector<Vertexette>::iterator it = new_vertex->edges.begin(),
                            end = new_vertex->edges.end(); it < end; ++it) {
                                if (connect->index == it->index) {
                                    connected = true;
                                    break;
                                }
                            }
                            if (connected) {
                                continue;
                            }
                        } else {
                            // Always try initial and goal first
                            connect = &vertices[tried_edges];
                        }
                    #else
                        connect = &vertices[tried_edges];
                    #endif
                    tried_edges++;
                    // Calculate number of steps between
                    dist = new_vertex->dist_to_normalised(connect);
                    if (dist < CONNECT_THRESHOLD) {
                        // Try to connect
                        if (is_visible(new_vertex, connect)) {
                            new_vertex->connect_to(connect->index, dist);
                            connect->connect_to(new_vertex->index, dist);
                            edges++;
                            new_edges++;
                        }
                    }
                }
            } else {
                if (corridor_config) {
                    corridor_configs_invalid_ob++;
                } else {
                    other_configs_invalid++;
                }
            }
        }
        after = clock();
        ms_graphing += 1000 * (after - before) / (double) CLOCKS_PER_SEC;

        before = clock();
        found_goal = astar(&vertices[0], &vertices[1]);
        after = clock();
        ms_searching += 1000 * (after - before) / (double) CLOCKS_PER_SEC;

    }
    cout << "vertices " << vertices.size() << endl;
    cout << "edges " << edges << endl;
    cout << "corridor_configs " << corridor_configs << endl;
    cout << "\tcorridor_configs_invalid_ws " << corridor_configs_invalid_ws << endl;
    cout << "\tcorridor_configs_invalid_ob " << corridor_configs_invalid_ob << endl;
    cout << "other_configs " << other_configs << endl;
    cout << "\tother_configs_invalid " << other_configs_invalid << endl;
}

double Navigator::astar_heuristic(Vertex *vertex, Vertex *goal) {
    if (vertex->cost_h == double_inf) {
        vertex->cost_h = vertex->dist_to_normalised(goal);
    }
    return vertex->cost_h;
}

bool Navigator::astar(Vertex *initial, Vertex *goal) {
    // Clear state
    for(vector<Vertex>::iterator it = vertices.begin(),
            end = vertices.end(); it != end; ++it) {
        it->visited = false;
        it->cost_f = double_inf;
        it->cost_g = double_inf;
        it->cost_h = double_inf;
        it->route_from = NULL;
        it->route_from_cost = double_inf;
    }

    // Set the initial vertex to 0 cost
    initial->cost_g = 0;
    initial->cost_f = initial->cost_g + astar_heuristic(initial, goal);

    // Make fringe queue
    priority_queue<Vertexette, vector<Vertexette>, greater<Vertexette>> fringe;
    fringe.push(Vertexette(initial->index, initial->cost_f));

    // Until the fringe is empty
    bool found_goal = false;
    while (fringe.size() > 0) {
        // Get and remove the cheapest fringe node
        Vertexette cheapest_vertexette = fringe.top();
        fringe.pop();

        // Check if it is the destination
        if (cheapest_vertexette.index == goal->index) {
            cout << "found goal" << endl;
            found_goal = true;
            break;
        }

        // Check if it hasn't been visited
        Vertex *cheapest = &vertices[cheapest_vertexette.index];
        if (!cheapest->visited) {
            // Visit it
            cheapest->visited = true;

            // For each of its neighbours
            for(vector<Vertexette>::iterator it = cheapest->edges.begin(),
                    end = cheapest->edges.end(); it != end; ++it) {
                // Get the neighbour
                Vertex *neighbour = &vertices[it->index];

                // Check if it hasn't been visited
                if (!neighbour->visited) {
                    // Calculate new cost
                    double new_cost_g = cheapest->cost_g + it->cost;

                    // Compare to current distance
                    if (new_cost_g < neighbour->cost_g) {
                        // Copy across new route
                        neighbour->cost_g = new_cost_g;
                        neighbour->route_from = cheapest;
                        neighbour->route_from_cost = it->cost;

                        // Update sort cost with heuristic
                        neighbour->cost_f = neighbour->cost_g +
                            astar_heuristic(neighbour, goal);

                        // Push it to fringe queue
                        fringe.push(Vertexette(it->index, neighbour->cost_f));
                    }
                }
            }
        }
    }

    return found_goal;
}
