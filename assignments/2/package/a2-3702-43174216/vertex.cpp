#include <iostream>
#include <iterator>
#include <valarray>
#include <vector>

#include "util.hpp"
#include "vertex.hpp"

using namespace std;

// Vertexette
bool operator>(const Vertexette &lhs, const Vertexette &rhs) {
    return lhs.cost > rhs.cost;
}

bool operator<(const Vertexette &lhs, const Vertexette &rhs) {
    return lhs.cost < rhs.cost;
}

ostream& operator<<(ostream &stream, const Vertexette &vertexette) {
    return stream << "<" << vertexette.index << ", " << vertexette.cost << ">";
    // return stream << "<" << vertexette.vertex->index << ", " << vertexette.cost << ">";
}

// Vertex
Vertex::Vertex(void) {
    index = -1;
    edge_count = 0;
    visited = false;
    cost_f = double_inf;
    cost_g = double_inf;
    cost_h = double_inf;
    route_from = NULL;
    route_from_cost = double_inf;
}

void Vertex::print_position(ostream &stream) {
    copy(begin(position), end(position), ostream_iterator<double>(cout, " "));
}

void Vertex::connect_to(int index, double cost) {
    edge_count++;
    edges.push_back(Vertexette(index, cost));
}

double Vertex::dist_to_normalised(Vertex *vertex) {
    valarray<double> diff = abs(vertex->normalised - normalised);
    diff[0] = sqrt(pow(diff[0], 2.0) + pow(diff[1], 2.0));
    return diff.max();
}

string Vertex::route_string(int *depth) {
    if (route_from) {
        (*depth)++;
        return route_from->route_string(depth) + "-" + to_string(index);
    }
    return to_string(index);
}

ostream& operator<<(ostream &stream, const Vertex &vertex) {
  return stream << "<" << vertex.index << " (" << vertex.cost_f << ")" << ">";
}
