#ifndef COLLISION_HPP
#define COLLISION_HPP

// Collision detectors (most checks should be no collision, I think)

#define COL_POINT_RECT(ax, ay, bx0, by0, bx1, by1) \
    (ax >= bx0 && ay >= by0 && ax <= bx1 && ay <= by1)

#define COL_RECT_RECT(ax0, ay0, ax1, ay1, bx0, by0, bx1, by1) \
    (!(ax0 > bx1 || ay0 > by1 || ax1 < bx0 || ay1 < by0))

// line-rect algorithms: https://en.wikipedia.org/wiki/Line_clipping
bool col_line_rect(double ax0, double ay0, double ax1, double ay1,
        double bx0, double by0, double bx1, double by1);

// line-line algorithms: stackoverflow
bool col_line_line(double ax0, double ay0, double ax1, double ay1,
        double bx0, double by0, double bx1, double by1);

#endif
