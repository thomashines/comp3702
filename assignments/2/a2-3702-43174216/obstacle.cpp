#include <sstream>
#include <string>
#include <math.h>

#include "obstacle.hpp"

using namespace std;

// void Obstacle::centre(void) {
//     cx = (x0 + x1) / 2.0;
//     cy = (y0 + y1) / 2.0;
//     r = sqrt(pow(x1 - cx, 2.0) + pow(y1 - cy, 2.0));
// }

// bool Obstacle::is_near(Obstacle *o) {
//     return sqrt(pow(cx - o->cx, 2.0) + pow(cy - o->cy, 2.0)) <= r + o->r;
// }

ostream& operator<<(std::ostream &stream, const Obstacle &o) {
    return stream << "[(" << o.x0 << "," << o.y0 << "),(" << o.x1 << "," << o.y1 << ")]";
}
