#ifndef CONFIG_HPP
#define CONFIG_HPP

// Robot dimensions
#define CHAIR_SIDE 0.041
#define LINK_LENGTH 0.05

// Dimension sizes
#define ANGLE_MIN -150.0 * (PI / 180.0)
#define ANGLE_MAX +150.0 * (PI / 180.0)
#define ANGLE_RANGE (ANGLE_MAX - ANGLE_MIN)
#define ANGLE_PRIM 0.099 * (PI / 180.0)
#define DIST_MAX 1.0
#define DIST_MIN 0.0
#define DIST_RANGE (DIST_MAX - DIST_MIN)
#define DIST_PRIM 0.00099
#define GRIP_MAX 0.07
#define GRIP_MIN 0.03
#define GRIP_RANGE (GRIP_MAX - GRIP_MIN)
#define GRIP_PRIM 0.00099
#define TOLERANCE 0.0001

// PRM settings
#define CONNECT_THRESHOLD 1000.0
// #define TRY_RANDOM_EDGES
#define TRY_EDGES_RATIO 1
#define NEW_EDGES -1
#define CORRIDOR_PERCENT 60

#endif
