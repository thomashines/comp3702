#include <fstream>
#include <sstream>
#include <string>
#include <valarray>

#include "util.hpp"

using namespace std;

// Max double value
double double_inf = numeric_limits<double>::max();

// Put file contents into string
string file_to_string(string file) {
    ifstream file_stream(file);
    file_stream.seekg(0, ios::end);
    size_t file_size = file_stream.tellg();
    string file_string(file_size, ' ');
    file_stream.seekg(0);
    file_stream.read(&file_string[0], file_size);
    return file_string;
}

// Get a string representation of a double valarray
string valarray_to_string(valarray<double> va) {
    ostringstream out;
    for (int i = 0; i < va.size(); i++) {
        out << va[i];
        if (i < va.size() - 1) {
            out << " ";
        }
    }
    return out.str();
}

// Returns a random double, for use in valarray::apply
double rand_double(double x) {
    return ((double) rand()) / RAND_MAX;
}

// Returns square root of a double, for use in valarray::apply
double sqrt_double(double x) {
    return pow(x, 0.5);
}

// Returns square of a double, for use in valarray::apply
double sq_double(double x) {
    return pow(x, 2.0);
}
