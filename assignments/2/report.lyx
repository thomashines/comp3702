#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
COMP3702 Assignment 2
\end_layout

\begin_layout Author
Thomas Hines
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

43174216
\end_layout

\begin_layout Section
Please define the Configuration Space of the problem and describe which
 method for searching in continuous space do you use.
\end_layout

\begin_layout Standard
The configuration space is the set of all configurations.
 As this is a continuous problem, it is a continuos space, with as many
 dimensions as the robot has degrees of freedom.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\#dimensions=\begin{cases}
2+\#joints & without\;gripper\\
6+\#joints & with\;gripper
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
The bounds of the configuration space are different for each dimension.
\end_layout

\begin_layout Standard
For the first 2 dimensions (chair x and y), the bounds are that of the workspace
:
\begin_inset Formula $[0,1]$
\end_inset

.
\end_layout

\begin_layout Standard
For the
\begin_inset Formula $\#joints$
\end_inset

 dimensions that make up the arm, the bounds are
\begin_inset Formula $[-150\textdegree,+150\textdegree]$
\end_inset

.
\end_layout

\begin_layout Standard
For the 4 dimensions that make up the gripper, the bounds are
\begin_inset Formula $[0.03,0.07]$
\end_inset

.
\end_layout

\begin_layout Standard
The forbidden regions in the configuration space are any points where:
\end_layout

\begin_layout Itemize
the geometry of the chair and arm collide with itself,
\end_layout

\begin_layout Itemize
or where the chair or arm collide with an object,
\end_layout

\begin_layout Itemize
or where the chair or arm extend beyond the workspace.
\end_layout

\begin_layout Standard
This continuous space is searched using a visibility graph.
 This graph is build by:
\end_layout

\begin_layout Enumerate
randomly sampling points within the configuration space bounds;
\end_layout

\begin_layout Enumerate
checking if these points are valid configurations;
\end_layout

\begin_layout Enumerate
making connections between the points where all the primitive steps between
 2 points are valid configurations.
\end_layout

\begin_layout Standard
A* is then used to search the visibility graph, where:
\end_layout

\begin_layout Itemize
the costs of the edges are the number of primitive steps required to move
 between the vertex configurations;
\end_layout

\begin_layout Itemize
and the heuristic is the number of primitive steps required to move directly
 to the goal vertex configuration from each vertex.
\end_layout

\begin_layout Section
If you use sampling-based method, please describe the strategy you apply
 or develop for each of its four components.
 Otherwise, please describe the details of your discretization method.
\end_layout

\begin_layout Subsection
Sampling strategy
\end_layout

\begin_layout Standard
Two sampling strategies are used to select candidate configurations, each
 time a new configuration is needed, one of these is chosen at random:
\end_layout

\begin_layout Enumerate
uniform random sampling 40% of the time, or
\end_layout

\begin_layout Enumerate
corridor random sampling 60% of the time.
\end_layout

\begin_layout Subsubsection
Uniform random sampling
\end_layout

\begin_layout Standard
A configuration is selected at random with every point in the configuration
 space having the same probability.
\end_layout

\begin_layout Subsubsection
Corridor random sampling
\end_layout

\begin_layout Standard
A list of corridors is made before the query is run.
 A corridor is selected at random from this list with a uniform probability
 distribution.
 A configuration is then selected using a normal disctribution for the chair
 x and y values and a uniform distribution for the joints and gripper.
\end_layout

\begin_layout Standard
When finding corridors, for each pair of obstacles the cases in
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Corridor-detection."

\end_inset

 are observed.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space \hfill{}
\end_inset


\begin_inset Graphics
	filename corridors.png
	lyxscale 10
	width 75text%

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Corridor-detection."

\end_inset

Corridor detection.
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
The normal distribution is specified with the following parameters, derived
 from
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Corridor-detection."

\end_inset

:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mu_{x}=\frac{x0+x1}{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\mu_{y}=\frac{y0+y1}{2}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sigma_{x}=\frac{x1-x0}{4}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\sigma_{y}=\frac{y1-y0}{4}
\]

\end_inset


\end_layout

\begin_layout Standard
This means that the majority of the configurations sampled should be within
 gaps between obstacles.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space \hfill{}
\end_inset


\begin_inset Graphics
	filename output/joint_6-obs_3.png
	lyxscale 10
	width 100text%

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Example-of-chair"

\end_inset

Example of chair x and y of the valid sampled configurations.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
A scatter plot of the chair x and y values of all the valid configurations
 sampled when solving the joint_6-obs_3 example problem has been provided
 in
\begin_inset CommandInset ref
LatexCommand formatted
reference "fig:Example-of-chair"

\end_inset

.
 The blank areas show where the obstacles are.
 The increased density in the gaps between obstacles is a result of the
 corridor random sampling strategy.
\end_layout

\begin_layout Subsection
Connection strategy
\end_layout

\begin_layout Standard
When a new vertex is added to the graph, edges between it and nearby vertices
 are created.
 To do this, the number of primitive steps to go from the new vertex to
 every other existing vertex is calculated.
 If the number of primitive steps is below a threshold, the edge is checked
 for validity, then added to the graph it is valid.
\end_layout

\begin_layout Standard
The threshold in use at the moment is 1000 primitive steps.
\end_layout

\begin_layout Subsection
Check if a configuration is valid or not
\end_layout

\begin_layout Standard
Each configuration is checked for collisions, the different cases of collision
 are:
\end_layout

\begin_layout Enumerate
chair extending beyond the workspace;
\end_layout

\begin_layout Enumerate
arm extending beyond the workspace;
\end_layout

\begin_layout Enumerate
arm colliding with the chair;
\end_layout

\begin_layout Enumerate
arm colliding with itself;
\end_layout

\begin_layout Enumerate
chair colliding with an obstacle;
\end_layout

\begin_layout Enumerate
arm colliding with an obstacle.
\end_layout

\begin_layout Standard
First, the geometry is generated from the configuration.
 Let the configuration be
\begin_inset Formula $c$
\end_inset

, an array of length
\begin_inset Formula $\#dimensions$
\end_inset

, with
\begin_inset Formula $c[0]$
\end_inset

 and
\begin_inset Formula $c[1]$
\end_inset

 being the chair x and y,
\begin_inset Formula $c[2..\#joints+1]$
\end_inset

 being the angles of the arm joints and
\begin_inset Formula $c[\#joints+2..\#joints+6]$
\end_inset

 being the gripper lengths.
\end_layout

\begin_layout Standard
The chair is defined by the bottom left and top right corners:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
(x0_{chair},y0_{chair})=(c[0]-0.02,c[1]-0.02)
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
(x1_{chair},y1_{chair})=(c[0]+0.02,c[1]+0.02)
\]

\end_inset


\end_layout

\begin_layout Standard
The arm is defined as a list of lines,
\begin_inset Formula $L$
\end_inset

, defined by their start and end points:
\begin_inset Formula $L_{i}=\{(x0_{Li},y0_{Li}),(x1_{Li},y1_{Li})\}$
\end_inset

.
 There are
\begin_inset Formula $\#joints$
\end_inset

 or
\begin_inset Formula $\#joints+4$
\end_inset

 lines in this list.
\end_layout

\begin_layout Standard
The list of obstacles,
\begin_inset Formula $O$
\end_inset

, is defined in the same way as the chair, by their bottom left and top
 right corners:
\begin_inset Formula $O_{i}=\{(x0_{Oi},y0_{Oi}),(x1_{Oi},y1_{Oi})\}$
\end_inset

.
\end_layout

\begin_layout Standard
The collision cases are handled as follows:
\end_layout

\begin_layout Subsubsection
Chair-workspace
\end_layout

\begin_layout Standard
This is not necesary to check, as the sampler's limits are set to be the
 limits of the workspace shrinked by half the side length of the chair.
\end_layout

\begin_layout Subsubsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Arm-workspace"

\end_inset

Arm-workspace
\end_layout

\begin_layout Standard
When calculating the vertices of the arm, the minimum and maximum x and
 y values are kept track of.
 Using these, it is easy to check if the arm is within the workspace:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x_{min}\geq0\;and\;y_{min}\geq0\;and\;x_{max}\leq1\;and\;y_{max}\leq1
\]

\end_inset


\end_layout

\begin_layout Subsubsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Arm-chair"

\end_inset

Arm-chair
\end_layout

\begin_layout Standard
The first link cannot collide with the chair, so for each
\begin_inset Formula $L_{i},i\geq1$
\end_inset

 (
\begin_inset Formula $L_{0}$
\end_inset

 is the first link), the line is checked for collision with the chair.
 This is first done by bounding boxes: if an axis-aligned bounding box around
 the line does not collide with the chair, then the link cannot collide
 with the chair.
 Next, if either the start or end point of the line is inside the chair
 box, then the line does collide with the chair.
 Finally, each line of the chair rectangle is checked for intersection with
 the link, if no intersection is found, then the link and chair do not collide.
\end_layout

\begin_layout Subsubsection
Arm-arm
\end_layout

\begin_layout Standard
Each link cannot collide with any link immediately adjacent to it, and nor
 can any link in the gripper collide with another link in the gripper.
 Therefore, for each link
\begin_inset Formula $L_{i},i\in\{0\;to\;\left|L\right|-6\}$
\end_inset

 (each link up to but not including the one before the gripper), check for
 collision with each link
\begin_inset Formula $L_{j},j\in\{i+2\;to\;\left|L\right|-1\}$
\end_inset

 (each link after it, including the gripper).
 If any of these lines collide, the configuration is invalid.
\end_layout

\begin_layout Subsubsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Chair-obstacle"

\end_inset

Chair-obstacle
\end_layout

\begin_layout Standard
For each obstacle, do a similar operation as the arm-workspace check, only
 inversed.
 Collision is not found if:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x0_{chair}>x1_{Oi}\;or\;y0_{chair}>y1_{Oi}\;or\;x1_{chair}<x0_{Oi}\;or\;y1_{chair}<y0_{Oi},i\in\{0\;to\;\left|O\right|-1\}
\]

\end_inset


\end_layout

\begin_layout Subsubsection
Arm-obstacle
\end_layout

\begin_layout Standard
For each obstacle, use the equation from
\begin_inset CommandInset ref
LatexCommand formatted
reference "subsec:Chair-obstacle"

\end_inset

 and the bounding box from
\begin_inset CommandInset ref
LatexCommand formatted
reference "subsec:Arm-workspace"

\end_inset

 to determine if the obstacle could collide with the arm.
 For each obstacle that can collide with the arm, follow the same process
 as in
\begin_inset CommandInset ref
LatexCommand formatted
reference "subsec:Arm-chair"

\end_inset

 but include the first link in the check to check for a collision between
 the arm and the obstacle.
\end_layout

\begin_layout Standard
If no collisions or problems are found in any of these cases, then the configura
tion is valid.
\end_layout

\begin_layout Subsection
Check if a line segment in C-space is valid or not
\end_layout

\begin_layout Standard
Line segments in configuration space are checked by discretising them with
 increasing resolution up to the primitive step level and checking if any
 of the discrete points along the line are invalid (if an invalid point
 is found, then the line segment is invalid).
\end_layout

\begin_layout Standard
First, the midpoint of the line is checked.
 Next, the midpoints of the 2 line segments between the 3 points (start,
 midpoint, end) are checked.
 Next, the midpoints of the 4 line segments are checked.
 More midpoints are checked until the distance between the checked points
 is less than or equal to a primitive step.
 If no invalid configurations have been found, then the line segment is
 valid.
\end_layout

\begin_layout Section
\begin_inset CommandInset label
LatexCommand label
name "sec:Which-class-of"

\end_inset

Which class of scenarios do you think your program will be able to solve?
 Please explain your answer.
\end_layout

\begin_layout Standard
I think my program can solve any scenario where a solution exists.
 The uniform random sampler has the potential to sample any possible configurati
on.
 So long as a sequence of possible configurations can be found that form
 a path from start to end satisfying the requirements of the assignment,
 a uniform random sampler will eventually sample every configuration on
 the path.
 This may take a uselessly massive amount of time and memory, but the software
 itself should eventually be able to solve the scenario.
\end_layout

\begin_layout Standard
Of the classes defined in the specification:
\end_layout

\begin_layout Enumerate
Just chair, no arm or gripper.
\end_layout

\begin_deeper
\begin_layout Enumerate
Tested an input with 9 obstacles.
 The program found a valid path in 580ms.
\end_layout

\end_deeper
\begin_layout Enumerate
Chair with at most 6 joints, no gripper.
\end_layout

\begin_deeper
\begin_layout Enumerate
Tested multiple inputs with 6 joints:
\end_layout

\end_deeper
\begin_layout Enumerate
Chair with at most 10 joints and gripper.
\end_layout

\begin_deeper
\begin_layout Enumerate
Tested multiple inputs: 5 joints in 18s; 6 joints in 28s; 7 joints in 94s;
 8 joints in 150s; 9 joints in 517s; 10 joints in 1967s; 11 joints in 7852s.
\end_layout

\end_deeper
\begin_layout Standard
Solutions have been found in all of these classes of scenarios.
 This means that if there is a factor that prevents my program from finding
 a solution, it is not defined in the scenario classifications.
\end_layout

\begin_layout Section
Under what situation do you think your program will fail? Please explain
 your answer.
\end_layout

\begin_layout Standard
The negative of my answer to 
\begin_inset CommandInset ref
LatexCommand formatted
reference "sec:Which-class-of"

\end_inset

: any situation where there is no solution.
 For example, where the initial or goal configurations are invalid, or where
 any path from the initial to the goal configuration is blocked by obstacles.
\end_layout

\begin_layout Standard
The time complexity of the program increases with the number of joints (estimate
d using the times listed in 
\begin_inset CommandInset ref
LatexCommand formatted
reference "sec:Which-class-of"

\end_inset

) at a rate of something like 
\begin_inset Formula $O(c^{n})$
\end_inset

, maybe 
\begin_inset Formula $O(4^{n})$
\end_inset

.
 Which means that as more joints are added, it is going to start taking
 a long time.
 For example, by 20 joints it might take 65 years to solve, which could
 be considered failure.
\end_layout

\end_body
\end_document
