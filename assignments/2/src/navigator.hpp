#ifndef NAVIGATOR_HPP
#define NAVIGATOR_HPP

#include <random>

#include "vertex.hpp"
#include "obstacle.hpp"

using namespace std;

class Navigator {
    public:
        bool gripper;
        int dimensions;
        int joints;
        int joints_end;
        valarray<double> min;
        valarray<double> max;
        valarray<double> rand_min;
        valarray<double> rand_max;
        valarray<double> rand_range;
        valarray<double> normaliser;
        vector<Obstacle> obstacles;
        Obstacle *chair_obstacle;
        vector<Obstacle> links;
        vector<Vertex> vertices;
        vector<double> corridor_x;
        vector<double> corridor_w_x;
        vector<double> corridor_y;
        vector<double> corridor_w_y;

        // RNG
        unsigned int seed;
        default_random_engine generator;
        normal_distribution<double> norm_dist_x;
        normal_distribution<double> norm_dist_y;

        // Timing
        double ms_startup;
        double ms_querying;
        double ms_graphing;
        double ms_searching;
        double ms_is_visible;
        double ms_is_valid;
        double ms_valarray;

        // Sampled configs file
        ofstream samples_stream;

        Navigator(char *query_file, char *out_file);
        bool is_valid(valarray<double> *config);
        bool is_visible(Vertex *from, Vertex *to);
        string motion_string(Vertex *to, int *steps);
        void prm_astar(valarray<double> *initial, valarray<double> *goal);
        double astar_heuristic(Vertex *vertex, Vertex *goal);
        bool astar(Vertex *initial, Vertex *goal);
};

#endif
