#ifndef OBSTACLE_HPP
#define OBSTACLE_HPP

using namespace std;

class Obstacle {
    public:
        double x0, y0, x1, y1;
        // double cx, cy, r;
        Obstacle(double x0, double y0, double x1, double y1):
            x0(x0), y0(y0), x1(x1), y1(y1) { }
            // x0(x0), y0(y0), x1(x1), y1(y1) { centre(); }
        Obstacle(void): x0(0), y0(0), x1(0), y1(0) { }
        // Obstacle(void): x0(0), y0(0), x1(0), y1(0), cx(0), cy(0), r(0) { }
        // void centre(void);
        // bool is_near(Obstacle *o);
};

ostream& operator<<(std::ostream &stream, const Obstacle &o);

#endif
