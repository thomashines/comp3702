package ttt;

/*
 */
public class Game {
    //
    private static String LINE_SEPARATOR = System.getProperty("line.separator");

    //
    public static int GAME_SIZE = 3;

    //
    private char[][] board;

    /*
     */
    public Game() {
        board = new char[GAME_SIZE][GAME_SIZE];
        clear();
    }

    /*
     */
    public void clear() {
        for (int r = 0; r < GAME_SIZE; r++) {
            for (int c = 0; c < GAME_SIZE; c++) {
                board[r][c] = ' ';
            }
        }
    }

    /*
     */
    public boolean play(char player, int row, int column) {
        if (row < 0 || row >= GAME_SIZE || column < 0 || column >= GAME_SIZE) {
            return false;
        }
        if (board[row][column] == ' ') {
            board[row][column] = player;
        } else {
            return false;
        }
        return true;
    }

    /*
     */
    public char getCell(int row, int column) {
        return board[row][column];
    }

    /*
     */
    public char getWinner() {
        char winner;

        // Check rows
        for (int r = 0; r < GAME_SIZE; r++) {
            winner = board[r][0];
            for (int c = 1; c < GAME_SIZE; c++) {
                if (board[r][c] != winner) {
                    winner = ' ';
                }
            }
            if (winner != ' ') {
                return winner;
            }
        }

        // Check columns
        for (int c = 0; c < GAME_SIZE; c++) {
            winner = board[0][c];
            for (int r = 1; r < GAME_SIZE; r++) {
                if (board[r][c] != winner) {
                    winner = ' ';
                }
            }
            if (winner != ' ') {
                return winner;
            }
        }

        // Check diagonals
        winner = board[1][1];
        if (winner != ' ') {
            if (board[0][0] == winner && board[2][2] == winner) {
                return winner;
            }
            if (board[2][0] == winner && board[0][2] == winner) {
                return winner;
            }
        }

        return ' ';
    }

    /*
     */
    public boolean over() {
        char winner = getWinner();
        if (winner != ' ') {
            return true;
        }

        for (int r = 0; r < GAME_SIZE; r++) {
            for (int c = 0; c < GAME_SIZE; c++) {
                if (board[r][c] == ' ') {
                    return false;
                }
            }
        }

        return true;
    }

    /*
     */
    @Override
    public String toString() {
        String result = "";
        result = result + "-----" + LINE_SEPARATOR;
        for (int r = 0; r < GAME_SIZE; r++) {
            result = result + "|";
            for (int c = 0; c < GAME_SIZE; c++) {
                result = result + board[r][c];
            }
            result = result + "|" + LINE_SEPARATOR;
        }
        result = result + "-----";
        return result;
    }
}
