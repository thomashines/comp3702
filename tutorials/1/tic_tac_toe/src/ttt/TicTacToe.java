package ttt;

import java.util.Scanner;

/**
 *
 */
public class TicTacToe {
    /**
     *
     */
    public static void main(String[] args) throws Exception {
        System.out.println("TicTacToe");

        Game game = new Game();

        Scanner reader = new Scanner(System.in);

        // Play games continuously
        for (;;) {
            // Clear the board
            game.clear();

            // Start turn loop
            while (!game.over()) {
                // User turn
                boolean played = false;
                while (!played) {
                    System.out.println("[rc]> ");
                    int rc = reader.nextInt();
                    played = game.play('o', rc / 10 - 1, rc % 10 - 1);
                }

                // Agent's turn
                agentPlay(game);

                // Print the board
                System.out.println(game.toString());
            }

            // Print the winner
            char winner = game.getWinner();
            if (winner == ' ') {
                System.out.println("nobody wins");
            } else {
                System.out.println(winner + " wins");
            }
        }
    }

    /*
     */
    private static void agentPlay(Game game) {
        // Get a score for each option
        int scores[][] = new int[game.GAME_SIZE][game.GAME_SIZE];
        for (int r = 0; r < game.GAME_SIZE; r++) {
            for (int c = 0; c < game.GAME_SIZE; c++) {
                if (game.getCell(r, c) == ' ') {
                    // board[r][c] = -100;
                } else {
                    // board[r][c] = 0;
                }
            }
        }
    }
}
