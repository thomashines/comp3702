#!/usr/bin/env python

### Tic Tac Toe Solver
import copy

board = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]

def print_board(b):
    print(b[0])
    print(b[1])
    print(b[2])

def board_full(b):
    for i in range(3):
        for j in range(3):
            if b[i][j] == ' ':
                return False
    return True

def all_o(i, j, k):
    return (i == 'o') and (j == 'o') and (k == 'o')

def all_x(i, j, k):
    return (i == 'x') and (j == 'x') and (k == 'x')

def victory_o(b):
    # check rows
    for n in range(3):
        if all_o(b[n][0], b[n][1], b[n][2]):
            return True
    # check cols
    for n in range(3):
        if all_o(b[0][n], b[1][n], b[2][n]):
            return True
    # check diags
    if all_o(b[0][0], b[1][1], b[2][2]):
        return True
    if all_o(b[0][2], b[1][1], b[2][0]):
        return True


def victory_x(b):
    # check rows
    for n in range(3):
        if all_x(b[n][0], b[n][1], b[n][2]):
            return True
    # check cols
    for n in range(3):
        if all_x(b[0][n], b[1][n], b[2][n]):
            return True
    # check diags
    if all_x(b[0][0], b[1][1], b[2][2]):
        return True
    if all_x(b[0][2], b[1][1], b[2][0]):
        return True

def heuristic(b):
    score = 0
    # check rows:
    for n in range(3):
        if (b[n][0] != 'o') and (b[n][1] != 'o') and (b[n][2] != 'o'):
            # this row is possible victory
            score += 1
        if (b[n][0] != 'x') and (b[n][1] != 'x') and (b[n][2] != 'x'):
            # this row is possible opponent victory
            score -= 1

    # check cols
    for n in range(3):
        if (b[0][n] != 'o') and (b[1][n] != 'o') and (b[2][n] != 'o'):
            # this col is possible victory
            score += 1
        if (b[0][n] != 'x') and (b[1][n] != 'x') and (b[2][n] != 'x'):
            # this col is possible opponent victory
            score -= 1

    # check diag 1
    if (b[0][0] != 'o') and (b[1][1] != 'o') and (b[2][2] != 'o'):
        score += 1
    if (b[0][0] != 'x') and (b[1][1] != 'x') and (b[2][2] != 'x'):
        score -= 1

    # check diag 2
    if (b[0][2] != 'o') and (b[1][1] != 'o') and (b[2][0] != 'o'):
        score += 1
    if (b[0][2] != 'x') and (b[1][1] != 'x') and (b[2][0] != 'x'):
        score -= 1

    return score

while True:
    # get input from user
    print('player move:')
    y = int(input('row>')) - 1
    x = int(input('col>')) - 1

    while(board[y][x] != ' '):
        print('/!\ invalid move')
        print('player move:')
        y = int(input('row>')) - 1
        x = int(input('col>')) - 1

    board[y][x] = 'o'

    print_board(board)

    if(victory_o(board)):
        print('=== player wins ===')
        exit()
    if(board_full(board)):
        print('=== tie ===')
        exit()

    # agent move
    print('agent move')
    # for each possible move, compute score
    best_score = -100
    best_move = (-1, -1)
    for i in range(3):
        for j in range(3):
            if board[i][j] == ' ':
                # move possible here
                b = copy.deepcopy(board)
                b[i][j] = 'x'
                score = heuristic(b)

                print('move:', (i, j))
                print('score:', score)

                if(score > best_score):
                    best_score = score
                    best_move = (i, j)

    board[best_move[0]][best_move[1]] = 'x'

    print_board(board)

    if(victory_x(board)):
        print('=== agent wins ===')
        exit()
    if(board_full(board)):
        print('=== tie ===')
        exit()

    print('==========')
